from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'app_cards'

urlpatterns = [
  path('<str:flow_id>/', views.CardView.as_view()),
  
]
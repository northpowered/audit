from app_index.models import User, Audit, Client
from app_workflow.northflow2 import Northflow
from pprint import pprint
from app_workflow.models import ClientFlowModel
from django.db.models import Q
class Card():
    def __init__(self, flow_id):
        self.flow_id = flow_id
        self.card = {}


    def get_owner_username(self, flow_id):
        try:
            flow = ClientFlowModel.objects.get(id = flow_id)
            if flow.client.role_level == Client.MAXIMAL:
                return flow.client.username
            else:
                return flow.client.master.username
        except ClientFlowModel.DoesNotExist:
            return None



    def get_client_tasks(self, username):
        result = {}
        try:
            client = Client.objects.get(username = username)
        except Client.DoesNotExist:
            result['success'] = False
            result['object'] = "Пользователь не является клиентом"
        else:
            flow = Northflow()
            client_flow = None
            client_id = None
            if client.role_level == Client.MAXIMAL:
                client_id = client.id
            else:
                boss = client.master
                client_id = boss.id

            client_flow = flow.get_client_flow(self.flow_id)
            if not client_flow['success']:
                result = client_flow
            else:
                client_flow = client_flow['object']['profile']
            tasks = []
            
            for block in client_flow['blocks']:
                for stage in block['questions']:
                    if stage['client_data']['linked_to'] == username and stage['client_data']['status'] == 'available':
                        stage_to_append = {}
                        stage_to_append['stage'] = stage
                        stage_to_append['parent_block'] = block['name']
                        tasks.append(stage_to_append)
            
            if len(tasks) == 0:
                result['success'] = False
                result['object'] = "Задачи не обнаружены"
            else:
                result['success'] = True
                result['object'] = tasks
        finally:
            
            return result
            
                

    def can_change_status(self, request_username, flow_id):
        request_user = User.objects.get(username = request_username)
        flow = ClientFlowModel.objects.get(id = flow_id)
        if request_user.role_class == User.ADMIN:
            return True
        elif request_user.role_class == User.AUDIT:
            #audit_user = Audit.objects.get(username = request_username)
            try:
                if flow.auditor.username == request_username:
                    return True
                else:
                    return False
            except KeyError:
                return False
                
        else:
            return False





    def get_slaves(self, request_user):
        try:
            boss = User.objects.get(username = request_user)
        except User.DoesNotExist:
            return None
        else:
            slaves = User.objects.filter(master = boss)
            #slaves = Client.objects.filter(Q(slave_workflow_link = str(flow_id)) | Q(username = boss_flow.client.username))
            #slaves = slaves.union(boss_flow.client)
            return slaves
            
    def can_view_card(self,flow_id, request_username):
        try:
            flow = ClientFlowModel.objects.get(id = flow_id)
            request_user = User.objects.get(username = request_username)
        except User.DoesNotExist:
            return False
        except ClientFlowModel.DoesNotExist:
            return False
        else:
            if flow.client.username == request_username: #can view selfcard
                return True
            elif request_user.role_class != User.CLIENT: #admins and auditors can view anything
                return True
            try:
                card_client = flow.client
                request_client = Client.objects.get(username = request_username)
            except Client.DoesNotExist:
                pass
            else:
                #if card_client.master == request_client: #master can view his slaves
               #     return True
                #print(request_client.master)
                #print(card_client)
                if request_client.master.username == card_client.username: #slaves can view their master
                    
                    return True
                else:
                    return False
            return False
        
            



    def get_flow_card(self, request_user = None):
        client_flow = None
        flow = Northflow()
        client_flow = flow.get_client_flow(self.flow_id)
        if not client_flow['success']:
            return client_flow
        client_flow['stat'] = flow.count_client_statistics(client_flow['object']['profile'])
        client_flow['completed'] = False
        if client_flow['stat']['green_pr'] == 100:
            client_flow['completed'] = True

        return client_flow




    def get(self, request_user = None):
        try:
            flow = ClientFlowModel.objects.get(id = self.flow_id)
        except ClientFlowModel.DoesNotExist:
            return None
        else:
            self.card['full'] = flow
            self.card['client'] = flow.client
            self.card['type'] = 'client'
            #self.card['slaves'] = self.get_slaves(self.flow_id)

            return self.card

    def get_linked_for_auditor(self, audit_username):
        flow = Northflow()
        result = flow.get_linked_clients(audit_username)
        return result

    def get_logs(self, username):
        flow = Northflow()
        result = flow.get_log_list(username)
        return result
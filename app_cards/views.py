from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import UserPassesTestMixin
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from app_index.models import User, Client
from app_certs.models import CertLevelModel
#from .permissons import RoleChecker
#from .slaves import AccountCreator
from django.http import HttpResponse,FileResponse
from .cards import Card
#from .apiprocessor import APIProcessor
from pprint import pprint
from app_workflow.northflow2 import Northflow
import json
from .forms import UploadFileForm
from app_workflow.reports import Report
from app_autocheck.casebook import CasebookAPI
@method_decorator(login_required, name='dispatch')
@method_decorator(csrf_exempt, name='dispatch')
class CardView(View):
    template_name = 'cards/card.html'

    def get(self, request, *args, **kwargs):
        request_user = request.user.username

        flow_id = kwargs['flow_id']
        card = Card(flow_id)
        permission_result =  card.can_view_card(flow_id, request_user)
        #print(permission_result)
        if not permission_result:
            return redirect('/')
        context = {}
        
        #user = User.objects.get(username = username)
        #context['who'] = user.role_class

        

        result = card.get()
        client_flow = card.get_flow_card()
        context['client_flow'] = client_flow
        #pprint(client_flow)
        context['card'] = result
        context['slaves'] = card.get_slaves(request_user)
        context['tasks'] = card.get_client_tasks(request_user)
        #context['linked_for_auditor'] = card.get_linked_for_auditor(username)
        context['can_deal_tasks'] = False
        context['can_change_status'] = card.can_change_status(request_user, flow_id)
        context['boss_username'] = card.get_owner_username(flow_id)
        context['is_boss'] = False
        context['is_slave'] = False
 
        if result['client'].username != result['client'].master.username:
            context['is_slave'] = True

        context['certlevels'] = CertLevelModel.objects.all()
        if context['boss_username'] == request.user.username:
            context['can_deal_tasks'] = True
            context['is_boss'] = True
        

        
        context['logs'] = card.get_logs(flow_id)
        ogrn = result['full'].ogrn
        apiHND = CasebookAPI()
        context['org_card'] = apiHND.get_organization_card(ogrn)
            

            



        return render(request, template_name=self.template_name, context=context)

    
    def post(self, request, *args, **kwargs):
        request_user = request.user.username
        mode = request.POST.get('mode','')
        flow_id = kwargs['flow_id']
       #pprint(request.FILES)
       # pprint(json.loads(request.POST.get('data')))
        if mode == 'link_stage':
            link_username = request.POST.get('username','')
            link_stage_id = request.POST.get('stage','')
            flow = Northflow()
            result = flow.link_worker_to_stage(request_user,link_username,link_stage_id,flow_id)
            return HttpResponse(result, content_type="application/json")
        elif mode == 'mass-link_stage':
            link_username = request.POST.get('username','')
            link_stages_id = json.loads(request.POST.get('stages',''))
            full_blocks = json.loads(request.POST.get('full_blocks',''))
            if len(full_blocks) == 0:
                full_blocks = None
            #pprint(full_blocks)
            flow = Northflow()
            result = flow.mass_link_worker_to_stage(request_user,link_username,link_stages_id,flow_id, full_blocks=full_blocks)
            return HttpResponse(result, content_type="application/json")
        elif mode == 'autocheck':
            flow = Northflow()
            result = flow.get_autochecks(flow_id)
            #print(result)
            return HttpResponse(result, content_type="application/json")
        elif mode == 'answer':
            context={}
            flow = Northflow()
            
            data = json.loads(request.POST.get('data'))
            #pprint(data)
            parsed_data={}
            for el in data:
                parsed_data[str(el['name'])]=str(el['value'])
            #pprint(parsed_data)
            result = flow.answer_question(request_user,parsed_data, request.FILES, flow_id)
            
            
            #print(result)
            return HttpResponse(json.dumps(result), content_type="application/json")
            #return redirect('/card/'+request_user)
        elif mode == 'change_status':
            flow = Northflow()
            stage = request.POST.get('stage','')
            new_status = request.POST.get('radiostatus','')
            owner = kwargs['flow_id']
            result = flow.change_stage_status(owner, str(stage), new_status, request_user=request_user)
            
            
            return redirect('/card/'+owner)
        elif mode == 'mass-status_change':
            flow = Northflow()
            stages = json.loads(request.POST.get('stages',''))
            new_status = request.POST.get('radiostatus','')
            
            owner = kwargs['flow_id']
            result = flow.mass_change_stage_status(owner, stages, new_status, request_user=request_user)
            return HttpResponse(result, content_type="application/json")
        elif mode == 'comment_stage':
            flow = Northflow()
            data = request.POST
            #pprint(data)
            
            result = flow.comment_stage(data)
            return HttpResponse(result, content_type="application/json")
        elif mode == 'report':
            report_type = request.POST.get('type','')
            result = {}
            
            if report_type == 'non_uploaded_files':
                client_id = request.POST.get('data','')
                report = Report()
                result = report.non_uploaded_files(client_id)
                return HttpResponse(result, content_type="application/json")
            elif report_type == 'risks_and_recs':
                client_id = request.POST.get('data','')
                return redirect("/workflow/report/risks/"+client_id)
                #report = Report()
                #result = report.risks_and_recs(client_id)
                #return HttpResponse(result, content_type="application/json")
        pass
        
from app_index.models import User, Client
import datetime
from django.core.mail import send_mail
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth import hashers
from django.db import IntegrityError
import re
from django.contrib.auth import hashers
import hashlib
import uuid

class ResetPassword():
    def __init__(self):
        pass

    def reset_password(self, login):
        result = {
            'success':True,
            'error':None
        }
        try:
            user = User.objects.get(username = login)
        except User.DoesNotExist:
            result['success'] = False
            result['error'] = 'Такой учетной записи не существует'
        else:
            salt = uuid.uuid4()
            base = str(salt) + str(login)
            password_encoded = hashlib.sha512(base.encode())
            password_cut = str(password_encoded.hexdigest())[:10]
            password_result =  hashers.make_password(password_cut)
            user.password = password_result
            email = user.email
            subject = 'Восстановление аккаунта на cert.cstandart.ru'
            message = '<p>Вы успешно восстановили пароль на сайте cert.cstandart.ru</p><p>Ваш временный пароль: '+password_cut+'</p><p>Если вы не регистрировались на сайте cert.ctandart.ru, то просто проигнорируйте это письмо</p>'
            try:
                send_mail(subject, 'Here is the message.', 'info@cstandart.ru',
                [email], fail_silently=False, html_message=message)
            except Exception as ex:
                result['success'] = False
                result['error'] = 'Ошибка отправки сообщения. Попробуйте еще раз'
            else:
                user.save()
        finally:
            return result
class ChangePassword():

    def __init__(self, username, old_password, password_1, password_2):
        self.username = username
        self.old_password = old_password
        self.password_1 = password_1
        self.password_2 = password_2
    
    def validate(self):

        user = authenticate(username=self.username, password=self.old_password)
        if user is None:
            return 'Неверный текущий пароль!'
        if len(self.password_1) < 8:
            return 'Длина пароля меньше 8 символов!'
        if len(self.password_2) < 8:
            return 'Длина пароля меньше 8 символов!'
        if self.password_1 != self.password_2:
            return 'Пароли не совпадают!'

        return None

    def change(self):

        valid = self.validate()
        if valid != None:
            return valid

        user = User.objects.get(username = self.username)
        password = hashers.make_password(self.password_1)
        user.password = password
        user.save()
        try:
            slave = Client.objects.get(username = self.username)
        except Client.DoesNotExist:
            pass
        else:
            slave.slave_changed_pwd = True
            slave.save()
        
        return None

class ConfirmAccount():

    def __init__(self, token):
        self.token = token
        

    def confirm(self):
        try:
            client_to_confirm = Client.objects.get(confirm_token = self.token)
        except Client.DoesNotExist:
            return 3
        if client_to_confirm.confirmed:
            return 2
        now = datetime.datetime.now()
        joined = client_to_confirm.date_joined
        if now.year != joined.year:
            return 1
        if now.month != joined.month:
            return 1
        if now.day != joined.day:
            return 1

        delta = datetime.timedelta(hours = now.hour - joined.hour, minutes = now.minute - joined.minute, seconds = now.second - joined.second)

        time_to_live = datetime.timedelta(hours = 1)

        if delta > time_to_live:
            return 1
        else:
            client_to_confirm.confirmed = True
            client_to_confirm.save()
            return 0

    def check_for_login(self, username):
        
        user_to_check = User.objects.get(username = username)
      
        if not user_to_check.confirmed:
            return 4 #не подтвержден
        if not user_to_check.approved:
            return 5 #не одобрен
        else:
            return 0 # все ок

    def check_for_first_change(self, username):

        try:
            slave = Client.objects.get(username = username)
            return slave.slave_changed_pwd
        except Client.DoesNotExist:
            return True



class RegisterAccount():

    def __init__(self, request):
        self.username = request.POST.get('username','').strip()
        self.password =  request.POST.get('pass','').strip()
        self.password2 =  request.POST.get('pass2','').strip()
       # self.companyname =  request.POST.get('companyname','').strip()
        #self.ogrn =  request.POST.get('ogrn','').strip()
       # self.address =  request.POST.get('address','').strip()
        self.first_name =  request.POST.get('first_name','').strip().capitalize()
        self.last_name =  request.POST.get('last_name','').strip().capitalize()
        self.father_name =  request.POST.get('father_name','').strip().capitalize()
        self.email =  request.POST.get('email','').strip()
        self.phone = request.POST.get('phone','').strip()
       # self.old_birthdate = request.POST.get('birthdate','').strip()
       # try:
       #     self.birthdate = datetime.datetime.strptime(request.POST.get('birthdate','').strip(),'%d-%m-%Y')
       # except ValueError:
       #     self.birthdate = None

    def validate(self):
        if not self.username.isalnum():
            return 'В логине должны быть только буквы и цифры!!'
        if not self.first_name.isalpha():
            return 'В имени должны быть только буквы!'
        if not self.last_name.isalpha():
            return 'В фамилии должны быть только буквы!'
       # if not self.ogrn.isdigit():
      #      return 'В поле ОГРН должны быть только цифры!'
        if not self.phone.isdigit():
            return 'В номере телефона должны быть только цифры!'
        if len(self.password) < 8:
            return 'Длина пароля меньше 8 символов!'
        if len(self.password2) < 8:
            return 'Длина пароля меньше 8 символов!'
        if self.password != self.password2:
            return 'Пароли не совпадают!'
        pattern = r"\"?([-a-zA-Z0-9.`?{}]+@\w+\.\w+)\"?"   
        if not re.match(pattern, self.email):
            return 'Неверный Email!'
        return None

    def send_confirm_message(self, token, email,first_name, last_name,father_name):
        #email = 'northpowered@gmail.com'
        subject = 'Подтверждение регистрации на cert.cstandart.ru'
        link = "https://cert.cstandart.ru/confirmation/"+str(token)
        message = '<p>Уважаемый '+last_name+' '+first_name+' '+father_name+', Вы успешно зарегистрировались на сайте cert.cstandart.ru</p><p>Нажмите на ссылку ниже, чтобы подтвердить факт регистрации. Помните, что для входа на сайт, ваш аккаунт должен дополнительно подтвердить администратор.</p><p><b>Ссылка: <a href="'+link+'">Нажмите для завершения регистрации</a></b></p><p>Если вы не регистрировались на сайте cert.ctandart.ru, то просто проигнорируйте это письмо</p>'
        #print('sending...')
        try:
            send_mail(subject, 'Here is the message.', 'info@cstandart.ru',
            [email], fail_silently=False, html_message=message)
        except Exception as ex:
            print(ex)
        #print('sent!')




    def register(self):
        if self.validate() != None:
            return self.validate()

        try:
    


            user = Client.objects.create_user(self.username, self.email, self.password)
            #user.companyname = self.companyname
            #user.ogrn = self.ogrn
            #user.address = self.address
            user.first_name = self.first_name
            user.last_name = self.last_name
            user.father_name = self.father_name
            user.email = self.email
            user.phone = self.phone
           # user.birthdate = self.birthdate
            user.role_class = User.CLIENT
            user.role_level = User.MAXIMAL
            user.confirmed = False
            user.approved = False
            user.slave_changed_pwd = True
            user.slave_workflow_link = 'foobar'
            user.save()
            user.master = user
            user.save()
            #print(user.confirm_token)
            self.send_confirm_message(user.confirm_token, self.email,self.first_name,self.last_name,self.father_name)

            return None
        except IntegrityError:
            return 'Этот логин уже занят!'


        
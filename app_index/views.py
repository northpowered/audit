from django.http.request import RAISE_ERROR
from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from app_accounts.permissons import RoleChecker
from .registration import RegisterAccount, ConfirmAccount, ChangePassword, ResetPassword
from app_workflow.northflow2 import Northflow 
import json

from django.http import HttpResponse
@method_decorator(login_required, name='dispatch')
class IndexView(View):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        context = {}
        username  = request.user.username
        #return redirect('/card/'+username)
        checker = RoleChecker()
        if checker.is_boss(username):
            flow = Northflow()
            result = flow.get_client_flows(username)
            context['who'] = 'boss'
            context['flows'] = result['flows']
        elif checker.is_slave(username):
            flow = Northflow()
            result = flow.get_slave_flows(username)
            context['who'] = 'slave'
            #context['flows'] = result['flows']
            try:
                context['flows'] = result['flows']
            except KeyError:
                 context['flows'] = None
        elif checker.is_audit(username):
            flow = Northflow()
            result = flow.get_audit_flows(username)
            context['who'] = 'audit'
            context['flows'] = result['flows']
        return render(request, template_name=self.template_name, context=context)


@method_decorator(login_required, name='dispatch')
class ChangePasswordView(View):
    template_name = 'changepassword.html'

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, template_name=self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        context = {}
        try:
            username = request.user.username
            old_password = request.POST.get('old_password','')
            password_1 = request.POST.get('password_1','')
            password_2 = request.POST.get('password_2','')
            ch_pwd = ChangePassword(username, old_password, password_1, password_2)
            result = ch_pwd.change()
            if result is not None:
                context['error'] = result
            else:
                logout(request)
                return redirect("/login/")
        except ValueError:
            print(1)
        return render(request, template_name=self.template_name, context=context)



class RegisterView(View):
    template_name = 'register.html'

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, template_name=self.template_name, context=context)

    def post(self, request, *args, **kwargs):
       

        RegObj = RegisterAccount(request)
        result = RegObj.register()
        #print(result)
        context = {}
        print(result)
        if result != None:
            to_response = {'success':False,'error':result}
        else:
            to_response = {'success':True}
        return HttpResponse(json.dumps(to_response), content_type="application/json")
        #return redirect("/endregister/")
    

class ConfirmationView(View):
    template_name = 'confirmation.html'

    def get(self, request, *args, **kwargs):
        context = {}
        token = kwargs['token']
        ConfirmObj = ConfirmAccount(token)
        result = ConfirmObj.confirm()
  
        if result == 0:
            context['result'] = True
        else:
            context['result'] = False
        return render(request, template_name=self.template_name, context=context)


class LoginView(View):
    template_name = 'login.html'
    
    def post(self, request, *args, **kwargs):
        login_error = None
        username = request.POST.get('login','')
        password = request.POST.get('password', '') 
        user = authenticate(username=username, password=password)
        if user is not None:
            ConfirmObj = ConfirmAccount('foobar')
            result = ConfirmObj.check_for_login(username=username)
            if result == 0:
                login(request, user)
                print('login OK!')
                if ConfirmObj.check_for_first_change(username):
                    return redirect('/')
                else:
                    return redirect('/changepassword')
            elif result == 4:
                login_error = 'Учетная запись не подтверждена по E-mail! Проверьте почту и пройдите по ссылке для подтверждения аккаунта!'
            elif result == 5:
                login_error = 'Учетная запись не одобрена! Подождите, пока нам менеджер проверить достоверность данных о вашей организации. Он может связаться с вами по указанному E-mail или номеру телефона'
        else:
            login_error = 'Ошибка! Проверьте корректность логина и пароля!'
        if login_error != None:
            #print(login_error)
            context = {'login_error': login_error}
            return render(request, template_name=self.template_name, context=context)


    def get(self, request, *args, **kwargs):
        return render(request, template_name=self.template_name)



class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/login/")

class EndRegView(View):
    def get(self, request, *args, **kwargs):
        return render(request, template_name="endregister.html")



class ResetPasswordView(View):
    template_name = 'resetpassword.html'

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, template_name=self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        result = {}
        try:
            login = request.POST.get('login','')
            hnd = ResetPassword()
            result = hnd.reset_password(login)
        except Exception:
            result = {
                'success':False,
                'error':'Неизвестная ошибка'
            }
        
        return HttpResponse(json.dumps(result), content_type="application/json")
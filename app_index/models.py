from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid


""" class RoleClassesModel(models.Model):
    class Meta:
        ordering = ['class_num']   
    ROLE_CLASSES = [
        ('admin', 'Администратор'),
        ('audit', 'Аудитор'),
        ('client', 'Клиент'),
    ]
    class_num = models.IntegerField(primary_key = True, editable = True)
    class_name = models.TextField(choices = ROLE_CLASSES, null = False)
    def __str__(self):
        return str(self.get_class_name_display())

class RoleLevelsModel(models.Model):
    class Meta:
        ordering = ['level_num'] 
    ROLE_LEVELS = [
        ('base', 'Базовый'),
        ('advanced', 'Продвинутый'),
        ('maximal', 'Максимальный'),
    ]
    level_num = models.IntegerField(primary_key = True, editable = True)
    level_name = models.TextField(choices = ROLE_LEVELS, null = False)
    def __str__(self):
        return str(self.get_level_name_display())
        #return str(self.level_num) """




class User(AbstractUser):
    class Meta:
        ordering = ['username'] 

    BASE = 'L1'
    ADVANCED = 'L2'
    MAXIMAL = 'L3'

    LEVEL_CHOICES = [
        (BASE, 'Базовый'),
        (ADVANCED, 'Продвинутый'),
        (MAXIMAL, 'Максимальный'),
    ]

    CLIENT = 'C1'
    AUDIT = 'C2'
    ADMIN = 'C3'

    CLASS_CHOICES = [
        (CLIENT, 'Клиент'),
        (AUDIT, 'Аудитор'),
        (ADMIN,'Администратор'),
    ]

    role_level = models.CharField(max_length=2, choices=LEVEL_CHOICES, default=BASE)
    role_class = models.CharField(max_length=2, choices=CLASS_CHOICES, default=CLIENT)

    #role_class = models.ForeignKey(RoleClassesModel, on_delete=models.CASCADE, null = True)
    #role_level = models.ForeignKey(RoleLevelsModel, on_delete=models.CASCADE, null = True)

    master = models.ForeignKey('self', on_delete=models.CASCADE, default = None, null = True, blank = True, related_name='relations')
    confirmed = models.BooleanField(default = False, null = False)
    approved = models.BooleanField(default = False, null = False)
    father_name = models.TextField(default = '', null = True, blank = True)
    phone = models.TextField(default = '', null = True, blank = True)
    birthdate = models.DateField(null = True, blank = True)

    confirm_token = models.UUIDField(default = uuid.uuid4, editable = True)


class Audit(User):
    class Meta:
        verbose_name = 'Auditor'

    description = models.TextField(null = True, blank = True)


class Client(User):

    class Meta:
        verbose_name = "Client"

    companyname = models.TextField(default = '', null = True, blank = True)
    ogrn = models.BigIntegerField(default = 0, null = True, blank = True)
    address = models.TextField(default = '', null = True, blank = True)
    payment = models.BooleanField(default = False, null = False)
    slave_workflow_link = models.TextField(default = '', null = False, blank = False)
    slave_changed_pwd = models.BooleanField(default = False, null = False)
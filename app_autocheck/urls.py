from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'app_autocheck'

urlpatterns = [
  url(r'^$', views.CheckView.as_view()),
  path('casebook/',views.CasebookView.as_view()),
  
]
from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import UserPassesTestMixin
from app_index.models import User, Client
from app_accounts.permissons import RoleChecker
from django.http import HttpResponse
from .checkprocessor import Processor
import json
from .casebook import CasebookAPI
@method_decorator(login_required, name='dispatch')
class CheckView(View):
    template_name = 'autocheck/autocheck.html'

    def get(self, request, *args, **kwargs):
        context = {}
        checker = RoleChecker()
        if checker.is_boss(request.user.username):
            client = Client.objects.get(username = request.user.username)
            context['client'] = client
            api = CasebookAPI()
            result = api.getData(client.ogrn)
            #print(json.loads(result)[0])
            #checks = Processor()
            #data = checks.process(client.ogrn)
            #if data is None:
            #    context['error'] = "Сервис не отвечает"
            context['checks'] = json.loads(result)[0]
            return render(request, template_name=self.template_name, context=context)

        else:
            return redirect('/')
        

class CasebookView(View):
    def get(self, request, *args, **kwargs):
        api = CasebookAPI()
        result = api.getData('1205700000604')
        #print(result)
        #result = {'foo':'bar'}
        #result = json.dumps(result)
        return HttpResponse(result, content_type="application/json")
from django.apps import AppConfig


class AppAutocheckConfig(AppConfig):
    name = 'app_autocheck'

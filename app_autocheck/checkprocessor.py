from audit import settings
import json
import requests

class Processor():

    def __init__(self):
        self.TEST_DATA_TRUE = """
            [
                {
                    "type": "massAddress",
                    "common_name": "Наличие в реестре массовых адресов",
                    "ok": true
                },

                {
                    "type": "massCEO",
                    "common_name": "Наличие в реестре массовых директоров",
                    "ok": true

                },

                {
                    "type": "bankrupt",
                    "common_name": "Наличие сведений о банкротстве компаний",
                    "ok": true

                },

                {
                    "type": "terrorists",
                    "common_name": "Наличие в списке террористических организаций",
                    "ok": true

                },

                {
                    "type": "disqualified",
                    "common_name": "Наличие учредителя в списке дисквалифицированных лиц",
                    "ok": true

                },
                {
                    "type": "dishonestsupplier",
                    "common_name": "Наличие в списке недобросовестных поставщиков",
                    "ok": true

                }
            ]
        """
        self.TEST_DATA_FALSE = """
                [

                    {
                        "type": "massAddress",
                        "common_name": "Наличие в реестре массовых адресов",
                        "ok": false,
                        "messages": [
                        "Адрес организации находится в реестре массовых адресов"
                            ]

                    },

                    {
                        "type": "massCEO",
                        "common_name": "Наличие в реестре массовых директоров",
                        "ok": false,
                        "messages": [
                        "Генеральный директор Камаз Ламаев находится в реестре массовых директоров"
                            ]

                    },

                    {
                        "type": "bankrupt",
                        "common_name": "Наличие сведений о банкротстве компаний",
                        "ok": false,
                        "messages": [
                        "Есть сведения о наличии процедур банкротства в отношении компании"
                            ]

                    },

                    {
                        "type": "terrorists",
                        "common_name": "Наличие в списке террористических организаций",
                        "ok": false,
                        "messages": [
                        "Учредитель Ушат Помоев находится в списке участников террористических организаций"
                            ]

                    },

                    {
                        "type": "disqualified",
                        "common_name": "Наличие учредителя в списке дисквалифицированных лиц",
                        "ok": false,
                        "messages": [
                        "Учредитель Поджог Сараев находится в списке дисквалифицированных лиц"
                            ]

                    },

                    {
                        "type": "dishonestsupplier",
                        "common_name": "Наличие в списке недобросовестных поставщиков",
                        "ok": false,
                        "messages": [
                        "Организация находится в списке недобросовестных поставщиков"
                            ]

                    }

                ]



        """

    def get_test_json(self, success):
        if success:
            return json.loads(self.TEST_DATA_TRUE)
        else:
            return json.loads(self.TEST_DATA_FALSE)


    def get_data_from_socket(self, OGRN):
        try:
            link = settings.AUTOCHECK_SCHEME+settings.AUTOCHECK_HOST+':'+str(settings.AUTOCHECK_PORT)+settings.AUTOCHECK_URL+str(OGRN)
            response = requests.get(link)
            return json.loads(response.text)
        except Exception:
            return None



    def generate_context(self, json_response):
        context = []
        if json_response is None:
            return None
        for block in json_response:
            one_check = {}
            
            try:
                if block['type']:
                    one_check['name'] = block['type']
                if block['common_name']:
                    one_check['common_name'] = block['common_name']
                    if block['ok']:
                        one_check['ok'] = True
                    else:
                        if block['messages']:
                            one_check['messages'] = block['messages']
                        else:
                            one_check['error'] = 'Нет данных о несоответствии'
                    context.append(one_check)

            except KeyError:
                one_check['ok'] = False
                one_check['error'] = 'Ошибка ответа сервера'

        return context

    def process(self, ogrn):
        raw_data = self.get_data_from_socket(ogrn)
        result = self.generate_context(raw_data)
        return result
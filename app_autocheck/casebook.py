import json
import requests
from audit import settings

class CasebookAPI():
    def __init__(self):
        self.TEST_GOOD = '1205700000604'
        self.TEST_BAD = '1147847254642'
        
        self.apiKeys = {}
        self.RISK_FACTORS = [
            {'link':'arbitrageMostlyRespondent','verbose':'Компания выступает в арбитражных делах преимущественно в роли ответчика'},
            {'link':'assetsDecreasedOrNotChanged','verbose':'Активы компании уменьшаются или не изменяются'},
            {'link':'authorizedCapitalDecreasedRecently','verbose':'Уменьшение уставного капитала компании менее года назад'},
            {'link':'authorizedCapitalGreaterOrEqualThanAssets','verbose':'Уставный капитал компании больше или равен активам'},
            {'link':'closeToMinimalAuthorizedCapital','verbose':'Размер уставного капитала близок к минимально разрешенному'},
            {'link':'currentChiefBankruptcyFns','verbose':'Текущий руководитель — банкрот или в процессе банкротства по версии ФНС'},
            {'link':'currentFounderBankruptcyEmitents','verbose':'Текущий владелец доли в уставном капитале — банкрот или в процессе банкротства по версии Эмитентов'},
            {'link':'currentFounderBankruptcyFns','verbose':'Текущий владелец доли в уставном капитале — банкрот или в процессе банкротства по версии ФНС'},
            {'link':'currentFounderBankruptcyRosstat','verbose':'Текущий владелец доли в уставном капитале — банкрот или в процессе банкротства по версии Росстат'},
            {'link':'currentFounderDisqualifiedEmitents','verbose':'Текущий владелец доли в уставном капитале внесен в реестр дисквалифицированных лиц по версии Эмитентов'},
            {'link':'currentFounderDisqualifiedFns','verbose':'Текущий владелец доли в уставном капитале внесен в реестр дисквалифицированных лиц по версии ФНС'},
            {'link':'currentFounderDisqualifiedRosstat','verbose':'Текущий владелец доли в уставном капитале внесен в реестр дисквалифицированных лиц по версии Росстат'},
            {'link':'doubtfulEgrulInformation','verbose':'Недостоверные сведения в ЕГРЮЛ'},
            {'link':'foundedOrganizationsBankruptcyFns','verbose':'Учрежденные компании - банкроты или в процессе банкротства по версии ФНС'},
            {'link':'founderOrganizationBankruptcyFns','verbose':'Компания-учредитель – банкрот или в процессе банкротства по версии ФНС'},
            {'link':'founderOrganizationBankruptcyRosstat','verbose':'Компания-учредитель – банкрот или в процессе банкротства по версии Росстат'},
            {'link':'hasDisqualifiedChiefs','verbose':'В состав исполнительных органов компании входят дисквалифицированные лица'},
            {'link':'hasNoRecentTaxReports','verbose':'Компания не предоставляет налоговую отчетность больше года'},
            {'link':'hasOldExecutoryProcesses','verbose':'Наличие активных исполнительных производств, зарегистрированных более двух месяцев назад'},
            {'link':'hasTaxDebtMoreThan1000','verbose':'Компания имеет взыскиваемую судебными приставами задолженность по уплате налогов, превышающую 1000 рублей'},
            {'link':'incomingClaimSumGreaterThanOutgoing','verbose':'Сумма предъявленных компании исков значительно превышает сумму предъявляемых ею исков'},
            {'link':'lastBooReportIsZero','verbose':'Последняя бухгалтерская отчетность компании содержит нулевые показатели'},
            {'link':'lastBooReportsAreEquals','verbose':'Последняя бухгалтерская отчетность компании совпадает с предыдущим периодом'},
            {'link':'massChiefCasebook','verbose':'Массовый руководитель по версии Casebook'},
            {'link':'massChiefFns','verbose':'Массовый руководитель по версии ФНС'},
            {'link':'massFounderCasebook','verbose':'Массовый учредитель по версии Casebook'},
            {'link':'massFounderFns','verbose':'Массовый учредитель по версии ФНС'},
            {'link':'massPhoneCasebook','verbose':'Массовый телефон по версии Casebook'},
            {'link':'massPhoneRosstat','verbose':'Массовый телефон по версии Росстат'},
            {'link':'massRegistrationAddressCasebook','verbose':'Массовый адрес регистрации по версии Casebook'},
            {'link':'massRegistrationAddressFns','verbose':'Массовый адрес регистрации по версии ФНС'},
            {'link':'minimalAuthorizedCapital','verbose':'Минимально разрешенный размер уставного капитала'},
            {'link':'organizationBankruptcy','verbose':'Компания – банкрот или в процессе банкротства'},
            {'link':'organizationBankruptcyIntention','verbose':'Компания намерена обратиться в суд с заявление о банкротстве'},
            {'link':'prosecutedForIllegalReward','verbose':'Компания привлечена к административной ответственности за незаконное вознаграждение'},
            {'link':'registeredRecently','verbose':'С даты регистрации компании прошло менее 365 дней'},
            {'link':'sanctionsList','verbose':'Компания включена в санкционные списки США'},
            {'link':'significantCurrentExecutoryProcessesSum','verbose':'Значительная сумма текущих исполнительных производств'},
            {'link':'significantlyIncreasedClaimLoad','verbose':'Значительное увеличение исковой нагрузки'},
            {'link':'tooManyChiefChanges','verbose':'Частая смена руководителя - более 2 раз за год'},
            {'link':'unfairSuppliers','verbose':'Компания внесена в реестр недобросовестных поставщиков'},
        ]

    def login(self):
        try:
            link = settings.CASEBOOK_SCHEME+settings.CASEBOOK_HOST+'/login'
            data = {'login':settings.CASEBOOK_LOGIN,'password':settings.CASEBOOK_PASSWORD}
            response = requests.post(link,json=data)
            if response.status_code is 200:
                self.apiKeys = response.json()
                #print(self.apiKeys['apiKeys'][0])
            else:
                pass
                #print(response.text)
            
            return response.status_code
        except Exception as e:
            return e


    def riskFactors(self):
        result = []
        for risk in self.RISK_FACTORS:
            data = {}
            link = settings.CASEBOOK_SCHEME+settings.CASEBOOK_HOST+'/organizations/'+str(self.OGRN)+'/riskFactors/'+risk['link']
            headers = {'apikey': self.apiKeys['apiKeys'][0]}
            response = requests.get(link, headers=headers)
            data = response.json()
            result.append(data)
        return result
    
    def riskFactor(self, factor):
        data = {}
        link = settings.CASEBOOK_SCHEME+settings.CASEBOOK_HOST+'/organizations/'+str(self.OGRN)+'/riskFactors/'+factor
        headers = {'apikey': self.apiKeys['apiKeys'][0]}
        response = requests.get(link, headers=headers)
        data = response.json()
        return data

    def get_organization_card(self, ogrn):
        #result = {}
        self.OGRN = ogrn
        self.login()
        data = {}
        link = settings.CASEBOOK_SCHEME+settings.CASEBOOK_HOST+'/organizations/'+str(self.OGRN)
        headers = {'apikey': self.apiKeys['apiKeys'][0]}
        response = requests.get(link, headers=headers)
        data = response.json()
        return data


    def getData(self, ogrn):
        result = []
        self.OGRN = ogrn
        self.login()

        risks = {'risks':self.riskFactors()}
        result.append(risks)
        return json.dumps(result)

    def getRisksList(self):
        raw_risks = self.RISK_FACTORS
        result = []
        for risk in raw_risks:
            new_risk = {}
            new_risk['value'] = risk['link']
            new_risk['label'] = risk['verbose']
            result.append(new_risk)
        return result

    def checkByList(self, stages, ogrn):
        self.OGRN = ogrn
        result = []
        self.login()
        for s in stages:
            result.append(self.riskFactor(s))
        return result

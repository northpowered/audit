from app_index.models import User, Client, Audit
from .permissons import RoleChecker
from django.contrib.auth import hashers
import hashlib
from django.core.mail import send_mail
from django.db import IntegrityError
from app_workflow.northflow2 import Northflow

class AccountCreator():

    def __init__(self, request):

        self.username = request.POST.get('username','').strip()
        self.first_name =  request.POST.get('first_name','').strip().capitalize()
        self.last_name =  request.POST.get('last_name','').strip().capitalize()
        self.father_name =  request.POST.get('father_name','').strip().capitalize()
        self.email =  request.POST.get('email','').strip()
        self.phone = request.POST.get('phone','').strip()
        self.link_name = request.POST.get('link_name','').strip()
        self.boss = request.user.username

    def validate(self):
        if not self.username.isalnum():
            return 'В логине должны быть только буквы и цифры!'
        if not self.first_name.isalpha():
            return 'В имени должны быть только буквы!'
        if not self.last_name.isalpha():
            return 'В фамилии должны быть только буквы!'
        if not self.phone.isdigit():
            return 'В номере телефона должны быть только цифры!'
        return None



    def generate_plain_password(self, uuid, username):
        plain = str(uuid) + username
        password = hashlib.sha512(plain.encode())
        #password = hashlib.sha1(str(password).encode())
        return str(password.hexdigest())[:10]


    def create_password(self, uuid, username):
        plain = self.generate_plain_password(uuid, username)
        #print(plain)
        #print(hashers.make_password(plain))
        return hashers.make_password(plain)


    def send_first_login_message(self, pwd):
        email = self.email
        subject = 'Приглашение на cert.cstandart.ru'
     
        message = '<b>password: '+pwd+'</b>'

        send_mail(subject, 'Here is the message.', 'info@cstandart.ru',
         [email], fail_silently=False, html_message=message)




    def create_audit(self, level, boss_name = None):

        valid = self.validate()
        if valid != None:
            return valid
        
        #role_class = RoleClassesModel.objects.get(class_name = 'audit')
        #role_level = RoleLevelsModel.objects.get(level_name = 'base')
        


        try:
            slave = Audit.objects.create_user(self.username, self.email, 'foobar')

            slave.first_name = self.first_name
            slave.last_name = self.last_name
            slave.father_name = self.father_name
            slave.email = self.email
            slave.phone = self.phone
            slave.birthdate = None
            slave.role_class = User.AUDIT
            slave.role_level = level
            slave.confirmed = True
            slave.approved = True

            if boss_name:
                try:
                    boss = Audit.objects.get(username = boss_name)
                    
                except Audit.DoesNotExist:
                    pass
            else:
                boss = None
            slave.master = boss
            
            slave.save()            
            password = self.create_password(slave.confirm_token, slave.username)
            print(password)
            slave.password = password
            slave.save()
            self.send_first_login_message(self.generate_plain_password(slave.confirm_token, self.username))
        except IntegrityError:
            return 'Такой логин уже существует!'
        return None





    def create_slave(self, by_admin = False, boss_name = ''):

        valid = self.validate()
        if valid != None:
            return valid
        
        
        


        try:
            slave = Client.objects.create_user(self.username, self.email, 'foobar')

            slave.first_name = self.first_name
            slave.last_name = self.last_name
            slave.father_name = self.father_name
            slave.email = self.email
            slave.phone = self.phone
            slave.birthdate = None
            slave.role_class = User.CLIENT
            slave.role_level = User.BASE
            slave.confirmed = True
            slave.approved = True
            
            boss_username = self.boss
            if by_admin:
                boss_username = boss_name



            
            try:
                boss = Client.objects.get(username = boss_username)
                slave.master = boss
                flow = Northflow()
                #flows = flow.get_client_flows(boss.username)
                #try:
                #    for f in flows['flows']:
                #        if f.link_name == self.link_name:
                #            slave.slave_workflow_link = f.id
                #except KeyError:
                #    return "Ошибка привязки"
                        
            except Client.DoesNotExist:
                    return "Ошибка присвоения руководителя"
            
                
            
            #slave.companyname = boss.companyname
            #slave.ogrn = boss.ogrn
            #slave.address = boss.address
            slave.save()            
            password = self.create_password(slave.confirm_token, slave.username)
            #print(password)
            slave.password = password
            slave.save()
            self.send_first_login_message(self.generate_plain_password(slave.confirm_token, self.username))
        except IntegrityError:
            return 'Такой логин уже существует!'
        return None






    def create_admin(self, boss_name):

        valid = self.validate()
        if valid != None:
            return valid
        
   

        try:
            slave = User.objects.create_user(self.username, self.email, 'foobar')

            slave.first_name = self.first_name
            slave.last_name = self.last_name
            slave.father_name = self.father_name
            slave.email = self.email
            slave.phone = self.phone
            slave.birthdate = None
            slave.role_class = User.ADMIN
            slave.role_level = User.BASE
            slave.confirmed = True
            slave.approved = True
            

            
            try:
                boss = User.objects.get(username = boss_name)
                slave.master = boss    
            except User.DoesNotExist:
                return "Ошибка присвоения руководителя"
            
            
            
            slave.save()            
            password = self.create_password(slave.confirm_token, slave.username)
            print(password)
            slave.password = password
            slave.save()
            self.send_first_login_message(self.generate_plain_password(slave.confirm_token, self.username))
        except IntegrityError:
            return 'Такой логин уже существует!'
        return None

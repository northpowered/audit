from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'app_accounts'
urlpatterns = [
  url(r'^$', views.AccountsView.as_view()),
 # path('getplain/<str:username>/', views.AccountsGetPlainView.as_view()),
  url(r'^admin', views.AdminAccountsTableView.as_view()),
  url(r'^client', views.ClientAccountsTableView.as_view()),
  path('api/<str:mode>/<str:user>/', views.AccountsAPIView.as_view()),
]
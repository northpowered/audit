from app_index.models import User, Client
from .permissons import RoleChecker
from django.db import IntegrityError

class APIProcessor():

    def __init__(self, user):
        self.user = user
    
    def approve(self):

        try:
            user = User.objects.get(username = self.user)
            user.approved = True
            user.save()
            return 0
        except User.DoesNotExist:
            return 1

    def ban(self):

        try:
            user = User.objects.get(username = self.user)
            user.is_active = not user.is_active
            user.save()
            return 0
        except User.DoesNotExist:
            return 1

    def delete(self):

        try:
            user = User.objects.get(username = self.user)
            user.delete()
            return 0
        except User.DoesNotExist:
            return 1
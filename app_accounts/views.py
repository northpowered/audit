from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import UserPassesTestMixin
from app_index.models import User, Client
from .permissons import RoleChecker
from .slaves import AccountCreator
from django.http import HttpResponse
from app_workflow.northflow2 import Northflow
from .apiprocessor import APIProcessor

@method_decorator(login_required, name='dispatch')
class AccountsView(View):
    #template_name = 'accounts/table.html'

    def get(self, request, *args, **kwargs):
        checker = RoleChecker()
        if checker.is_boss(request.user.username):
            return redirect('/accounts/client')
        if checker.is_admin(request.user.username):
            return redirect('/accounts/admin')
        else:
            return redirect('/')
        

@method_decorator(login_required, name='dispatch')
class AccountsAPIView(View):

    def get(self, request, *args, **kwargs):
        checker = RoleChecker()
        #print(kwargs['mode'])
       #print(kwargs['user'])
        try:
            source_user = User.objects.get(username = request.user.username)
            target_user = User.objects.get(username = kwargs['user'])
            #print(source_user)
            #print(target_user)
        except User.DoesNotExist:
            return ('/')
        if checker.is_admin(request.user.username):
            if kwargs['mode'] == 'approve':
                processor = APIProcessor(kwargs['user'])
                processor.approve()
            if kwargs['mode'] == 'ban':
                processor = APIProcessor(kwargs['user'])
                processor.ban()
            if kwargs['mode'] == 'delete':
                processor = APIProcessor(kwargs['user'])
                result = processor.delete()
                #print(result)
            return redirect('/accounts/admin')
            
        elif checker.is_boss(request.user.username) and source_user == target_user.master:
            if kwargs['mode'] == 'ban':
                processor = APIProcessor(kwargs['user'])
                processor.ban()
                f = Northflow()
                f.log_action(request.user.username,request.user.username,'warning','subclient', 'ban', {'subclient':kwargs['user']})
            if kwargs['mode'] == 'delete':
                processor = APIProcessor(kwargs['user'])
                result = processor.delete()
                f = Northflow()
                f.log_action(request.user.username,request.user.username,'warning','subclient', 'delete', {'subclient':kwargs['user']})
            return redirect('/accounts/client')




@method_decorator(login_required, name='dispatch')
class AdminAccountsTableView(View):
    template_name = 'accounts/table.html'


    def get(self, request, *args, **kwargs):

        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')

        context = {}
        accounts = User.objects.all()
        context['accounts'] = accounts
        context['levels'] = User.LEVEL_CHOICES
        context['classes'] = User.CLASS_CHOICES
        return render(request, template_name=self.template_name, context=context)
        
    def post(self, request, *args, **kwargs):
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')

        context = {}
        accounts = User.objects.all()
        context['accounts'] = accounts
        context['levels'] = User.LEVEL_CHOICES
        context['classes'] = User.CLASS_CHOICES

        mode = request.POST.get("mode","")

        if mode == "addclient":
            slave = AccountCreator(request)
            result = slave.create_slave(by_admin=True, boss_name=request.POST.get("boss",""))
            if result:
                    context['error'] = result
        
        if mode == "addaudit":
            raw_level = request.POST.get("level","")

            boss = request.POST.get("boss","")
            level = None
            for l in User.LEVEL_CHOICES:
                if raw_level in l:
                    level = l[0]
                    
            slave = AccountCreator(request)
            result = slave.create_audit(level, boss_name=boss)
            if result:
                    context['error'] = result

        if mode == "addadmin":
            slave = AccountCreator(request)
            result = slave.create_admin(boss_name=request.POST.get("boss",""))
            if result:
                    context['error'] = result

        return render(request, template_name=self.template_name, context=context)


@method_decorator(login_required, name='dispatch')
class ClientAccountsTableView(View):
    template_name = 'accounts/client.html'


    def get(self, request, *args, **kwargs):

        checker = RoleChecker()
        if checker.is_boss(request.user.username):
            context = {}
            client = Client.objects.get(username = request.user.username)
            context['client'] = client
            context['levels'] = User.LEVEL_CHOICES
            context['classes'] = User.CLASS_CHOICES
            flow = Northflow()
            context['flows'] = flow.get_client_flows(request.user.username)
            return render(request, template_name=self.template_name, context=context)
        else:
            return redirect('/')

    def post(self, request, *args, **kwargs):
        context = {}
        checker = RoleChecker()
        mode = request.POST.get('mode','')

        if checker.is_boss(request.user.username):
            if mode == 'add':
                new_slave = AccountCreator(request)
                result = new_slave.create_slave()
                if result:
                    context['error'] = result
                #f = Northflow()
                #f.log_action(request.user.username,request.user.username,'warning','subclient', 'creation', {'subclient':request.POST.get('username','')})


            client = Client.objects.get(username = request.user.username)
            context['client'] = client
            return render(request, template_name=self.template_name, context=context)

            #return render(request, template_name=self.template_name, context=context)
        else:
            return redirect('/')


@method_decorator(login_required, name='dispatch')
class AccountsGetPlainView(View):
    def get(self, request, *args, **kwargs):
        response = HttpResponse('')
        
        try:
            username = kwargs['username']
            client = Client.objects.get(username = username)
            uuid = client.confirm_token
            slave = AccountCreator(request)
            plain = slave.generate_plain_password(uuid, username)
            response = HttpResponse(plain)

        except Client.DoesNotExist:
            pass

        return response
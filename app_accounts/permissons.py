from app_index.models import User, Client, Audit


class RoleChecker():

    def __init__(self):
        pass

    def is_client(self, username):
        try:
            client = Client.objects.get(username = username)
        except Client.DoesNotExist:
            return False
        return True

    def is_slave(self, username):
        if not self.is_client(username):
            return False
        client = Client.objects.get(username = username)
        
        if client.role_level != User.MAXIMAL or client.master.username != username:
            return True
        else:
            return False

    def is_admin(self, username):
        try:
            user = User.objects.get(username = username)
        except User.DoesNotExist:
            return False
        if(user.role_class == User.ADMIN):
            return True
        else:
            return False

    def is_boss(self, username):
        if not self.is_client(username):
            return False
        client = Client.objects.get(username = username)
        
        if client.role_level == User.MAXIMAL:
            return True
        else:
            return False
             
    def is_audit(self, username):
        try:
            audit = Audit.objects.get(username = username)
        except Audit.DoesNotExist:
            return False
        return True
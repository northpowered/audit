from django.db import models
import uuid
import datetime
import django.utils
from app_index.models import User



class CertLevelModel(models.Model):

    class Meta:
        ordering = ['num_level']
        
    num_level = models.IntegerField(primary_key = True, editable = True)
    text_level = models.TextField(null = False)

    def __str__(self):
        return str(self.text_level)


class CertModel(models.Model):
    class Meta:
        ordering = ['-certnum']
        permissions = (
            ('certR', "Read cert table"),
            ('certW', "Write cert table"),
            ('certD', "Delete cert table"),
            ('certC', "Change cert table"),
        )
    certnum = models.AutoField(primary_key = True)
    companyname = models.TextField(null = False)
    profile_name = models.TextField(null = True, default=None)
    profile_id = models.TextField(null = True, default=None)
    ogrn = models.BigIntegerField(null = False)
    level = models.ForeignKey(CertLevelModel, on_delete=models.CASCADE)
    score = models.IntegerField(null = False, default = 0)
    certcreatedate = models.DateField(null = False)
    certenddate = models.DateField(null = False)
    who_create = models.ForeignKey(User, on_delete = models.SET_NULL, blank = True, null = True,related_name='who_create')
    who_approved = models.ForeignKey(User, on_delete = models.SET_NULL, blank = True, null = True,related_name='who_approved')
    approved = models.BooleanField(default = False, null = False)
    active = models.BooleanField(default = True, null = False)

    def __str__(self):
        return str(str(self.certnum) + ": "+str(self.companyname)+"/"+str(self.profile_name))

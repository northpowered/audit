from django.conf.urls import url

from . import views

app_name = 'app_certs'
urlpatterns = [
  url(r'^$', views.FrontCertView.as_view()),
  url(r'^table', views.TableCertView.as_view()),
]
from pprint import pprint
from app_index.models import User
from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from .models import CertModel, CertLevelModel
from .certmodule import CertOperations
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from app_accounts.permissons import RoleChecker
import json
from app_workflow.northflow2 import Northflow

from django.http import HttpResponse
class FrontCertView(View):
    template_name = 'certs/front.html'

    def get(self, request, *args, **kwargs):
        return redirect('/') #TEMP!
        context = {}
        return render(request, template_name=self.template_name, context=context)


@method_decorator(login_required, name='dispatch')
class TableCertView(View):
    template_name = 'certs/table.html'
    


    def get(self, request, *args, **kwargs):
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')
        context = {}
        flow = Northflow()
        context['flows_from_version'] = flow.get_flows_from_version()
        certs = CertModel.objects.all()
        context['levels'] = CertLevelModel.objects.all()
        current_page = Paginator(certs,15)
        page = request.GET.get('page')
        try:
            context['certs'] = current_page.page(page)  
        except PageNotAnInteger:
            context['certs'] = current_page.page(1)  
        except EmptyPage:
            context['certs'] = current_page.page(current_page.num_pages) 
        return render(request, template_name=self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')
        context = {}
        send={}
        #pprint(request.POST.getlist("public-methods[]",[]))
        mode = request.POST.get('mode','')
        send['companyname'] = request.POST.get('companyname','')
        send['ogrn'] = request.POST.get('ogrn','')
        send['certnum'] = request.POST.get('certnum','')
        send['level'] = request.POST.get('level','')
        send['score'] = request.POST.get('score','')
        send['certcreatedate'] = request.POST.get('certcreatedate','')
        send['certenddate'] = request.POST.get('certenddate','')
        cert_operation = CertOperations(send)


        if mode == 'add':
            #cert_operation.add()
            profiles_to_add = ", ".join(request.POST.getlist("public-methods[]",[]))
            
           
            cert_operation.insert(request.POST.get('level',''),request.POST.get('companyname',''),request.POST.get('ogrn',''),request.POST.get('score',''),None,None,profiles_to_add,None,request.user.username)
        elif mode == 'edit':
            cert_operation.edit()
        elif mode == 'delete':
            cert_operation.delete()
        elif mode == 'approve':
            result = cert_operation.approve(certnum=request.POST.get('certnum',''),username=request.user.username)
            return HttpResponse(json.dumps(result), content_type="application/json")
        elif mode == 'active':
            result = cert_operation.activate(certnum=request.POST.get('certnum',''),username=request.user.username)
            return HttpResponse(json.dumps(result), content_type="application/json")
        else:
            pass




        if cert_operation.error != None:
            context['error'] = cert_operation.error

        
        flow = Northflow()
        context['flows_from_version'] = flow.get_flows_from_version()
        certs = CertModel.objects.all()
        context['levels'] = CertLevelModel.objects.all()
        current_page = Paginator(certs,15)
        page = request.GET.get('page')
        try:
            context['certs'] = current_page.page(page)  
        except PageNotAnInteger:
            context['certs'] = current_page.page(1)  
        except EmptyPage:
            context['certs'] = current_page.page(current_page.num_pages) 
        return render(request, template_name=self.template_name, context=context)

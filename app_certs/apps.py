from django.apps import AppConfig


class AppCertsConfig(AppConfig):
    name = 'app_certs'

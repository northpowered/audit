from django.contrib import admin
from .models import CertModel, CertLevelModel

admin.site.register(CertModel)
admin.site.register(CertLevelModel)
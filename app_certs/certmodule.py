from typing import Type
from .models import CertModel, CertLevelModel
from django.db import IntegrityError
from django.core.exceptions import ValidationError
from app_index.models import User
import datetime
class CertOperations():

    def __init__(self, send):
        self.send = send
        self.error = None
        

    def approve(self, certnum, username):
        result = {}
        try:
            user = User.objects.get(username=username)
            cert = CertModel.objects.get(certnum = int(certnum))
            if not user.is_staff:
                result['success'] = False
                result['error'] = "Запрещено!"
                return result
            cert.approved = True
            cert.who_approved = user
            dt_now = datetime.datetime.now()
            cert.certcreatedate = dt_now
            cert.certenddate = dt_now.replace(year = dt_now.year + 1)
            cert.save()
        except User.DoesNotExist:
            result['success'] = False
            result['error'] = "Пользователь не найден"
        except CertModel.DoesNotExist:
            result['success'] = False
            result['error'] = "Сертификат не найден"
        except TypeError:
            result['success'] = False
            result['error'] = "Ошибка в номере сертификата"
        else:
            result['success'] = True
            result['user'] = username
        finally:
            return result

    def activate(self, certnum, username):
        result = {}
        try:
            user = User.objects.get(username=username)
            cert = CertModel.objects.get(certnum = int(certnum))
            if not user.is_staff:
                result['success'] = False
                result['error'] = "Запрещено!"
                return result
            cert.active = not cert.active
            cert.save()
        except User.DoesNotExist:
            result['success'] = False
            result['error'] = "Пользователь не найден"
        except CertModel.DoesNotExist:
            result['success'] = False
            result['error'] = "Сертификат не найден"
        except TypeError:
            result['success'] = False
            result['error'] = "Ошибка в номере сертификата"
        else:
            result['success'] = True
            result['active'] = cert.active
        finally:
            return result


    def insert(self, level, companyname, ogrn, score, certcreatedate, certenddate, profile_name, profile_id, who_create):
        try:
            if not ogrn.isdigit():
                raise ValidationError('ogrn')
            if not str(score).isdigit():
                raise ValidationError('score')
            user = User.objects.get(username=who_create)
            
            new_cert_level = CertLevelModel.objects.get(text_level = level)
            new_cert = CertModel.objects.create(companyname = companyname,
                ogrn = ogrn,
                level = new_cert_level,
                score = score,
                certcreatedate = datetime.datetime.now(),
                certenddate = datetime.datetime.now(),
                profile_name = profile_name,
                profile_id = profile_id,
                who_create = user)
            new_cert.save()
            return new_cert.certnum
        except IntegrityError:
            self.error = "Невозможно добавить сертификат. Возможно, такой номер уже существует!"
        except CertLevelModel.DoesNotExist:
            self.error = "Проверьте уровень сертификата!"
        except User.DoesNotExist:
            self.error = "Несуществующий создатель сертификата!"
        except ValidationError:
            self.error = "Проверьте входные данные!"
            #print(self.error)
        except Exception as ex:
            print(ex)


    def get(self, id):
        result = {}
        try:
            cert = CertModel.objects.get(certnum = str(id))
            result['success'] = True
            result['object'] = {}
            result['object']['id'] = cert.certnum
            result['object']['companyname'] = cert.companyname
            result['object']['score'] = cert.score
            result['object']['certcreatedate'] = str(cert.certcreatedate)
            result['object']['certenddate'] = str(cert.certenddate)
            result['object']['level'] = {}
            result['object']['level']['num'] = str(cert.level.num_level)
            result['object']['level']['text'] = str(cert.level.text_level)
        except CertModel.DoesNotExist:
            result['success'] = False
            result['details'] = "Такого объекта не существует!"
        finally:
            return result


    def add(self):
        try:
            new_cert_level = CertLevelModel.objects.get(text_level = self.send['level'])
            new_cert = CertModel.objects.create(certnum = self.send['certnum'], companyname = self.send['companyname'], ogrn = self.send['ogrn'], level = new_cert_level, score = self.send['score'], certcreatedate = self.send['certcreatedate'], certenddate = self.send['certenddate'])
            new_cert.save()
        except IntegrityError:
            self.error = "Невозможно добавить сертификат. Возможно, такой номер уже существует!"
        except CertLevelModel.DoesNotExist:
            self.error = "Проверьте уровень сертификата!"
    
    def edit(self):
        try:
            edit_cert = CertModel.objects.get(certnum = self.send['certnum'])
            edit_cert.companyname = self.send['companyname']
            edit_cert.ogrn = self.send['ogrn']
            edit_cert_level = CertLevelModel.objects.get(text_level = self.send['level'])
            edit_cert.level = edit_cert_level
            edit_cert.score = self.send['score']
            edit_cert.certcreatedate = self.send['certcreatedate']
            edit_cert.certenddate = self.send['certenddate']
            edit_cert.save()
        except IntegrityError:
            self.error = "Невозможно изменить данные! Возможно, Вам стоит проверить входные данные!"
        except ValueError:
            self.error = "Невозможно изменить данные! Возможно, Вам стоит проверить входные данные!"
        except CertLevelModel.DoesNotExist:
            self.error = "Проверьте уровень сертификата!"

    def delete(self):
        try:
            del_cert = CertModel.objects.get(certnum = self.send['certnum'])
            del_cert.delete()
        except CertModel.DoesNotExist:
            self.error = "Такого объекта для удаления уже не существует!"


from django.shortcuts import render
from django.views import View
from django.http import FileResponse

class GetMediaFile(View):
    def get(self, request, *args, **kwargs):

        username = kwargs['username']
        filename = kwargs['filename']
        
        full_filename = 'files/clients/'+username+'/'+filename


        response = FileResponse(open(full_filename, 'rb'))
        response['Content-Disposition'] = 'inline; filename='+filename
        return response
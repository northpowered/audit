from django.shortcuts import render, get_object_or_404, redirect, HttpResponse
from django.http import Http404
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from app_certs import models
import pdfkit
from django.http import FileResponse
from pprint import pprint
from contextlib import suppress
from pyvirtualdisplay import Display
class GetView(View):
    template_name = 'get/index.html'

    def get(self, request, *args, **kwargs):
        
        cert_num = kwargs['num']
        cert = None

        if request.GET.get('format','') == 'pdf':
            url = str(request._get_scheme())+'://'+request._get_raw_host()+'/get/'+str(cert_num)
            filename = 'render_'+str(cert_num)+'.pdf'
            full_filename = 'files/tmp/'+filename
           # Я ЗНАЮ, НО ЭТО ТАК НАДО
            try:
                # ЭТО ГОВНО ПОДНИМАЕТ OSError на обработчики RGB но метод отрабатывает
                with Display():
                    pdfkit.from_url(url,full_filename,options={'page-size':'B4'})
            except OSError:
                pass
            finally:
                response = FileResponse(open(full_filename, 'rb'))
                response['Content-Disposition'] = 'inline; filename='+filename
                return response
            # ВОТ ДО СЮДА
        

        try:
            context = {}
            cert = models.CertModel.objects.get(certnum = cert_num)
            if not (cert.approved and cert.active):
                raise Http404
            #print(cert.level.num_level)
            #template_name = 'get/index.html'
            if str(cert.level.num_level) == '0':
                self.template_name = 'get/sert2.html'
            elif str(cert.level.num_level) == '1':
                self.template_name = 'get/sert3.html'
            elif str(cert.level.num_level) == '2':
                self.template_name = 'get/sert1.html'
            else:
                pass
            context['cert'] = cert
            response = render(request, template_name=self.template_name, context=context)
            return response
        except models.CertModel.DoesNotExist:
            raise Http404
            

        


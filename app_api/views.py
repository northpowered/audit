from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.views import View
from .apiprocessor import APIProcessor
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import json
from django.http import FileResponse

# Create your views here.
@method_decorator(csrf_exempt, name='dispatch')
class APIView(View):
    #template_name = 'accounts/table.html'

    def get(self, request, *args, **kwargs):
        
        return redirect('/')
    
    def post(self, request, *args, **kwargs):
        received_json_data = json.loads(request.body.decode("utf-8"))
        
        processor = APIProcessor()
        mode = received_json_data["mode"]
        token = received_json_data["token"]
        result = processor.router(mode, token, received_json_data)
        if result['type'] == 'file':
            response = FileResponse(open(result['filename'], 'rb'))
            response['Content-Disposition'] = 'inline; filename='+result['common_name']
            return response
        
        
from audit.settings import API_AUTH_TOKEN
from django.http import HttpResponseForbidden, HttpResponseBadRequest
from app_certs.certmodule import CertOperations
import uuid
import datetime
from django.http import FileResponse
import qrcode
class APIProcessor():

    def __init__(self):
        pass

    def router(self, mode, token, args):
        if token != API_AUTH_TOKEN:
            
            return HttpResponseForbidden()
        if mode == 'questioning':
            result = self.questioning(args)

        return result

    def questioning(self, args):
        name = args["name"]
        if name == "":
            return HttpResponseBadRequest
        level = "Базовый"
        certnum = str(uuid.uuid4())
        ogrn = 122334454
        score = 100
        certcreatedate = datetime.date.today()
        certenddate = datetime.date.today() 
        cert = CertOperations('foobar')
        cert.insert(level, certnum, name, ogrn, score, certcreatedate, certenddate)
        
        link = "https://cert.cstandart.ru/get/"+certnum
        common_name = certnum+'.png'
        filename = 'files/tmp/qr-'+common_name
        qr = qrcode.QRCode(
        version=2,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
        )
        qr.add_data(link)
        qr.make(fit=True)

        img = qr.make_image(fill_color="black", back_color="white")
        img = img.convert('RGB') 
        img.save(filename)
        result = {}
        result['type'] = "file"
        result['filename'] = filename
        result['common_name'] = common_name
        return result
        

        



from django.apps import AppConfig


class AppQrgenConfig(AppConfig):
    name = 'app_qrgen'

from django.shortcuts import render
from django.views import View
from django.http import FileResponse
import qrcode


class QRGeneratorView(View):

    def get(self, request, *args, **kwargs):
        crt_num = request.GET.get("crt")
        link = "https://cert.cstandart.ru/get/"+crt_num
        common_name = crt_num+'.png'
        filename = 'files/tmp/qr-'+common_name
        qr = qrcode.QRCode(
        version=2,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
        )
        qr.add_data(link)
        qr.make(fit=True)

        img = qr.make_image(fill_color="black", back_color="white")
        img = img.convert('RGB') 
        img.save(filename)
        response = FileResponse(open(filename, 'rb'))
        response['Content-Disposition'] = 'inline; filename='+common_name
        return response


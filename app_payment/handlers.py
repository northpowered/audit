from .models import *
import json
from .serializers import *
from django.core.exceptions import ValidationError
class RateHandler():

    def __init__(self):
        pass

    def getAllRates(self):
        rates = RateSerializer(RateModel.objects.all(), many=True)
        return rates.data

    def addRate(self, data):
        result = {}
        try:
            new_rate = RateModel.objects.create(name = data['name'], description = data['description'], cost = data['cost'], is_free = data['is_free'], is_active = data['is_active'])
            new_cert.save()
        except ValidationError:
            result['success'] = False
            result['error'] = "Проверьте входные данные!"
        except ValueError:
            result['success'] = False
            result['error'] = 'Проверьте входные данные!'
        else:
            result['success'] = True
        finally:
            return result

    def editRate(self, data):
        result = {}
        try:
            rate = RateModel.objects.get(id = data['id'])
            rate.name = data['name']
            rate.description = data['description']
            rate.cost = data['cost']
            rate.is_free = data['is_free']
            rate.save()
        except ValidationError:
            result['success'] = False
            result['error'] = "Проверьте входные данные!"
        except RateModel.DoesNotExist:
            result['success'] = False
            result['error'] = "Проверьте входные данные!"
        else:
            result['success'] = True
        finally:
            return result

    def deleteRate(self, id):
        result = {}
        try:
            rate = RateModel.objects.get(id = id)
            rate.delete()
        except RateModel.DoesNotExist:
            result['success'] = False
            result['error'] = "Проверьте входные данные!"
        else:
            result['success'] = True
        finally:
            return result

    def switchActiveRate(self, id):
        result = {}
        try:
            rate = RateModel.objects.get(id = id)
            rate.is_active = not rate.is_active
            rate.save()
        except RateModel.DoesNotExist:
            result['success'] = False
            result['error'] = "Проверьте входные данные!"
        else:
            result['success'] = True
        finally:
            return result
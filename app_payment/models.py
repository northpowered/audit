from django.db import models
import uuid


class RateModel(models.Model):
    class Meta:
        ordering = ['name']
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(null = False, blank = False, unique = True)
    description = models.TextField(null = False, blank = False)
    cost = models.PositiveIntegerField(default = 0)
    is_free = models.BooleanField(default = False, null = False)
    is_active = models.BooleanField(default = False, null = False)

    def __str__(self):
        return str(self.name)
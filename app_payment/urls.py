from django.urls import path
from .views import *

app_name = "app_payment"
# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path(r'', RateTemplateView.as_view()),
    path('rate/',RateView.as_view()),
    path('parser/',ParserView.as_view()),
]
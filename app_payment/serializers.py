from rest_framework import serializers

class RateSerializer(serializers.Serializer):

    id = serializers.UUIDField(format='hex_verbose')
    name = serializers.CharField()
    description = serializers.CharField()
    cost = serializers.IntegerField()
    is_free = serializers.BooleanField()
    is_active = serializers.BooleanField()

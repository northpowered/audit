from django.shortcuts import render, redirect
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from .handlers import *
from app_accounts.permissons import RoleChecker
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from django.http import HttpResponse
from .parser import *
class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return None

class RateTemplateView(View):
    template_name = 'payment/table.html'

    def get(self, request, *args, **kwargs):
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')
        context = {}
        hnd = RateHandler()
        rates = hnd.getAllRates()
        current_page = Paginator(rates,15)
        page = request.GET.get('page')
        try:
            context['rates'] = current_page.page(page)  
        except PageNotAnInteger:
            context['rates'] = current_page.page(1)  
        except EmptyPage:
            context['rates'] = current_page.page(current_page.num_pages) 
        return render(request, template_name=self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')
        mode = request.POST.get('mode','')
        if mode == 'add':
            #print(request.POST)
            hnd = RateHandler()
            data = {}
            data['name'] = request.POST.get('name','')
            data['description'] = request.POST.get('description','')
            data['cost'] = request.POST.get('cost','')
            try:
                if request.POST.get('is_free','') == 'on':
                    data['is_free'] = True
                else:
                    data['is_free'] = False
            except KeyError:
                data['is_free'] = False
            data['is_active'] = False
            result = hnd.addRate(data)
            context = {}
            try:
                if not result['success']:
                    context['error'] = result['error']
            except KeyError:
                pass
            rates = hnd.getAllRates()
            current_page = Paginator(rates,15)
            page = request.GET.get('page')
            try:
                context['rates'] = current_page.page(page)  
            except PageNotAnInteger:
                context['rates'] = current_page.page(1)  
            except EmptyPage:
                context['rates'] = current_page.page(current_page.num_pages)
            return render(request, template_name=self.template_name, context=context)
        elif mode == 'edit':
            #print(request.POST)
            data = {}
            data['id'] = request.POST.get('id','')
            data['name'] = request.POST.get('name','')
            data['description'] = request.POST.get('description','')
            data['cost'] = request.POST.get('cost','')
            try:
                if request.POST.get('is_free','') == 'on':
                    data['is_free'] = True
                else:
                    data['is_free'] = False
            except KeyError:
                data['is_free'] = False
            data['is_active'] = False
            hnd = RateHandler()
            result = hnd.editRate(data)
            #print(result)
            context = {}
            try:
                if not result['success']:
                    context['error'] = result['error']
            except KeyError:
                pass
            rates = hnd.getAllRates()
            current_page = Paginator(rates,15)
            page = request.GET.get('page')
            try:
                context['rates'] = current_page.page(page)  
            except PageNotAnInteger:
                context['rates'] = current_page.page(1)  
            except EmptyPage:
                context['rates'] = current_page.page(current_page.num_pages)
            return render(request, template_name=self.template_name, context=context)
        elif mode == 'delete':
            #print(request.POST)
            hnd = RateHandler()
            result = hnd.deleteRate(request.POST.get('id',''))
            #print(result)
            return redirect('/payment')
        elif mode == 'activate':
            #print(request.POST)
            hnd = RateHandler()
            result = hnd.switchActiveRate(request.POST.get('id',''))
            return redirect('/payment')
class RateView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        hnd = RateHandler()
        result = hnd.getAllRates()
        return Response(result)



class ParserView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        hnd = PaymentParser()
        lines = hnd.load_file('testbilling.txt')
        result = hnd.load_from_lines(lines)
        return Response(result)
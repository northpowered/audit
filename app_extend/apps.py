from django.apps import AppConfig


class AppExtendConfig(AppConfig):
    name = 'app_extend'

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.views import View
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
# Create your views here.
@method_decorator(login_required, name='dispatch')
class TestView(View):
    template_name = 'extend_base.html'

    def get(self, request, *args, **kwargs):
        context = {}
        #context['app_name'] = '/load/testapp'
        return render(request, template_name=self.template_name, context=context)
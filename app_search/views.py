from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView
from app_workflow.northflow2 import Northflow
import json
from django.http import Http404
from rest_framework.exceptions import NotFound
from app_search.search import CertSearch
from django.views import View
from django.shortcuts import render
class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return None

class CertSearchFormView(View):
    template_name = 'search/form.html'

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, template_name=self.template_name, context=context)


class CertOGRNSearchView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):

        result = {}
        try:
            status = 200
            hnd = CertSearch()
            
            ogrn = kwargs['ogrn']
            certs = hnd.search(ogrn)
            if len(certs) == 0:
                status = 404
                result['success'] = False
                result['details'] = 'No certs with such OGRN'
            else:
                result['success'] = True
                result['certs'] = certs
        except KeyError:
            status = 404
            result = {
                "success" : False,
                "details" : "No certs with such OGRN"
            }
        except Exception as ex:
            pass
        else:
            pass
            
        finally:
            return Response(result, status=status)

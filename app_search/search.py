from app_certs.models import CertModel, CertLevelModel

class CertSearch():

    def __init__(self):
        pass

    def search(self, ogrn):
        result = []
        try:
            certs = CertModel.objects.filter(ogrn=ogrn, active = True, approved = True)
        except Exception as ex:
            pass
        else:
            for cert in certs:
                s_cert = {
                    'num': cert.certnum,
                    'companyname': cert.companyname,
                    'level': cert.level.text_level,
                    'certcreatedate': cert.certcreatedate,
                    'certenddate': cert.certenddate,
                    'profile_name': cert.profile_name,
                }
                result.append(s_cert)
        finally:
            return result
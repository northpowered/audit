from django.urls import path
from .views import *

app_name = "app_search"
# app_name will help us do a reverse look-up latter.
urlpatterns = [
   # path(r'', RateTemplateView.as_view()),
    path('form/',CertSearchFormView.as_view()),
    path('cert/',CertOGRNSearchView.as_view()),
    path('cert/<str:ogrn>',CertOGRNSearchView.as_view()),
   # path('parser/',ParserView.as_view()),
]
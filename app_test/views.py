from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator


@method_decorator(login_required, name='dispatch')
class TestView(View):
    template_name = 'test/test.html'

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, template_name=self.template_name, context=context)

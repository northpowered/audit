from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'app_test'
urlpatterns = [
  url(r'^$', views.TestView.as_view()),

 # url(r'^admin', views.AdminAccountsTableView.as_view()),
#  url(r'^client', views.ClientAccountsTableView.as_view()),
 # path('api/<str:mode>/<str:user>/', views.AccountsAPIView.as_view()),
]
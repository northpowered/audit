# Generated by Django 3.0.3 on 2020-05-18 14:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_workflow', '0009_auto_20200518_1009'),
    ]

    operations = [
        migrations.AddField(
            model_name='workflowmodel',
            name='branched_from',
            field=models.UUIDField(default=None, null=True),
        ),
    ]

from django.shortcuts import render, get_object_or_404, redirect, HttpResponse
from django.http import Http404
from django.views import View
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from app_accounts.permissons import RoleChecker
#from .northflow import Workflow, API
import json
from pprint import pprint
import datetime
from app_workflow import reports
from .northflow2 import Northflow
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from app_workflow.reports import Report
import pdfkit
from django.http import FileResponse
from pyvirtualdisplay import Display
@method_decorator(login_required, name='dispatch')
class WorkflowMainView(View):
    template_name = 'workflow/index.html'

    def get(self, request, *args, **kwargs):
        checker = RoleChecker()
        if checker.is_admin(request.user.username):
            return redirect('/workflow/table')
        return redirect('/')

@method_decorator(login_required, name='dispatch')
class WorkflowTableView(View):
    template_name = 'workflow/table.html'
    def get(self, request, *args, **kwargs):
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')
        context = {}
        flow = Northflow()

        context['versions'] = flow.get_all_objects()
        current_page = Paginator(context['versions'],15)
        page = request.GET.get('page')
        try:
            context['versions'] = current_page.page(page)  
        except PageNotAnInteger:
            context['versions'] = current_page.page(1)  
        except EmptyPage:
            context['versions'] = current_page.page(current_page.num_pages)
        if len(context['versions']) == 0:
            context['empty'] = True
        
        context['active_version'] = flow.get_active_version()
        
        return render(request, template_name=self.template_name, context=context)
    
    def post(self, request, *args, **kwargs):
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')
        context = {}
        flow = Northflow()
        mode = request.POST.get("mode","")
        if mode == 'active':
            uid = request.POST.get("uid","")
            result = flow.activate_version(uid)
            print(result)
            return redirect('/workflow/table')
        return render(request, template_name=self.template_name, context=context)
        

@method_decorator(login_required, name='dispatch')
class WorkflowEditView(View):
    template_name = 'workflow/edityaml.html'

    def get(self, request, *args, **kwargs):
       
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')
        context = {}
        flow = Northflow()
         
        uid = kwargs['uuid']
        if uid == "new":
            context['new'] = True
            context['object'] = {'id': 'new'}
        elif uid == 'None':
            return redirect('/workflow/table')
        else:
            context['object'] = flow.load_from_db(uid)
            context['yaml'] = flow.render_to_frontend(context['object'].data_bin)

        return render(request, template_name=self.template_name, context=context)


    def post(self, request, *args, **kwargs):
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')
        mode = request.POST.get("mode","")
        flow = Northflow()
        if mode == 'validate':
            data = request.POST.get("data","")
            result = flow.validate(data)
            return HttpResponse(result, content_type="application/json")
        elif mode == 'iter':
            data = request.POST.get("data","")
            uid = kwargs['uuid']
            result = flow.iterate_flow(uid, data, request.user.username)
            return HttpResponse(result, content_type="application/json")

@method_decorator(login_required, name='dispatch')
class WorkflowClientsView(View):
    template_name = 'workflow/clients.html'

    def get(self, request, *args, **kwargs):
        context = {}
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')

        flow = Northflow()
       # context['active_version'] = flow.get_active_version()
        context['flows_from_version'] = flow.get_flows_from_version()
        #pprint(context['flows_from_version'])
        client_list = flow.get_clients_list()
        client_flows = flow.get_client_workflow_list()
        context['client_flows'] = client_flows
        context['client_list'] = client_list

        if len(client_flows) == 0:
            context['empty'] = True

        #full_list = flow.get_all_objects()
        
        #context['versions'] = full_list
        context['auditors'] = flow.get_all_auditors()
        return render(request, template_name=self.template_name, context=context)
    
    def post(self, request, *args, **kwargs):
        checker = RoleChecker()
        if not checker.is_admin(request.user.username):
            return redirect('/')
        mode = request.POST.get("mode","")
        flow = Northflow()
        if mode == 'create':
            username = request.POST.get("username","")
            version = request.POST.getlist("version[]",[])
            link_name = request.POST.get("link_name","")
            ogrn = request.POST.get("ogrn","")
            #print(version)
            #print(version)
            result = None
            for v in version:
                result = flow.create_client_flow(username, v, link_name, ogrn)
                #print(result)
            
            #if result['success']:
            #    flow.log_action(username, request.user.username, 'info', 'profile' , 'creation' , {'version': version,'profile_name':result['flow_name']})
            result = json.dumps(result)
            return HttpResponse(result, content_type="application/json")
        if mode == 'link_audit':
            flow_id = request.POST.get("flow","")
            audit = request.POST.get("audit","")
            result = flow.link_audit_to_client(flow_id,audit)
            pprint(result)
            if json.loads(result)['success']:
                flow.log_action(flow_id, request.user.username, 'info', 'profile' , 'audit_link' , {'auditor': audit})
            return HttpResponse(result, content_type="application/json")

class ReportRisksView(View):
    template_name = 'reports/risksrecs.html'
    def get(self, request, *args, **kwargs):
        context = {}
        if request.GET.get('format','') == 'pdf':
            url = str(request._get_scheme())+'://'+request._get_raw_host()+'/workflow/report/risks/'+str(kwargs['id'])
            filename = 'risks_'+str(kwargs['id'])+'.pdf'
            full_filename = 'files/reports/'+filename
           # Я ЗНАЮ, НО ЭТО ТАК НАДО
            try:
                # ЭТО ГОВНО ПОДНИМАЕТ OSError на обработчики RGB но метод отрабатывает
                if not request.GET.get('mode','') == 'new':
                    response = FileResponse(open(full_filename, 'rb'))
                else:
                    raise FileNotFoundError
            except FileNotFoundError:
                if request.GET.get('mode','') == 'new':
                    try:
                        with Display():
                            pdfkit.from_url(url,full_filename,options={'page-size':'A4'})
                    except OSError as ex:
                        pass
                else:
                    raise Http404
            finally:
                try:
                    response = FileResponse(open(full_filename, 'rb'))
                    response['Content-Disposition'] = 'inline; filename='+filename
                    return response
                except FileNotFoundError:
                    raise Http404
        hnd = Report()
        report = hnd.risks_and_recs(str(kwargs['id']))
        if not report['success']:
            raise Http404
        context['report'] = report['object']
        context['ogrn'] = report['ogrn']
        context['org_name'] = report['org_name']
        context['report_date'] = datetime.datetime.now()
        return render(request, template_name=self.template_name, context=context)

class ReportNonuploadView(View):
    template_name = 'reports/nonupload.html'
    def get(self, request, *args, **kwargs):
        context = {}
        if request.GET.get('format','') == 'pdf':
            url = str(request._get_scheme())+'://'+request._get_raw_host()+'/workflow/report/nonupload/'+str(kwargs['id'])
            filename = 'nonupload_'+str(kwargs['id'])+'.pdf'
            full_filename = 'files/reports/'+filename
           # Я ЗНАЮ, НО ЭТО ТАК НАДО
            try:
                # ЭТО ГОВНО ПОДНИМАЕТ OSError на обработчики RGB но метод отрабатывает
                if not request.GET.get('mode','') == 'new':
                    response = FileResponse(open(full_filename, 'rb'))
                else:
                    raise FileNotFoundError
            except FileNotFoundError:
                if request.GET.get('mode','') == 'new':
                    try:
                        
                         with Display():
                            pdfkit.from_url(url,full_filename,options={'page-size':'A4'})
                        
                    except OSError:
                        pass
                else:
                    raise Http404
            finally:
                try:
                    response = FileResponse(open(full_filename, 'rb'))
                    response['Content-Disposition'] = 'inline; filename='+filename
                    return response
                except FileNotFoundError:
                    raise Http404
        hnd = Report()
        report = hnd.non_uploaded_files(str(kwargs['id']))
        if not report['success']:
            raise Http404
        context['report'] = report['object']
        context['report_date'] = datetime.datetime.now()
        return render(request, template_name=self.template_name, context=context)
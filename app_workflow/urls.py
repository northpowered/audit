from django.conf.urls import url, include
from django.urls import path
from . import views
from django.views import generic


app_name = 'app_workflow'
urlpatterns = [

  url(r'^$', views.WorkflowMainView.as_view()),
  #url(r'^edit', views.WorkflowEdit2View.as_view()),
  path('table/', views.WorkflowTableView.as_view()),
  path('clients/', views.WorkflowClientsView.as_view()),
  path('edit/<str:uuid>/', views.WorkflowEditView.as_view()),
  path('report/risks/<str:id>/', views.ReportRisksView.as_view()),
  path('report/nonupload/<str:id>/', views.ReportNonuploadView.as_view()),
 # path('api/<str:mode>/<str:stage>/', views.WorkflowAPIView.as_view()),
  
]
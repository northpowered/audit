import yaml
import json
import collections
import yamlloader
import sys
import os
from pprint import pprint
from .models import NewWorkflowModel, WorkflowModel, ClientFlowModel
from app_index.models import Client, Audit
from app_accounts.permissons import RoleChecker
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import IntegrityError
from app_autocheck.checkprocessor import Processor
from app_autocheck.casebook import CasebookAPI
from uuid import UUID
from datetime import datetime
from app_workflow import northdebug


class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return obj.hex
        return json.JSONEncoder.default(self, obj)

class Northflow():

    def __init__(self):
        self.testyaml = """
flow:
  - stage: 1
    type: 'simple'
    name: 'name1'
    common_name: 'Этап №1'
    description: 'sjfbjhbfgkjsdfikjvf'

  - stage: 2
    type: 'question'
    name: 'name2'
    common_name: 'Этап №2'
    description: 'fs54365464564566456'
    question_type: 'radio'
    answers:
      - {text: 'ans1', file: 0}
      - {text: 'ans2', file: 0}
      - {text: 'ans3', file: 1}
    
        """


        pass

    def load_from_yaml(self, yaml_data):
        result = yaml.load(yaml_data,Loader=yamlloader.ordereddict.CSafeLoader)
        #pprint(result)
        return result
        
    def dump_to_yaml(self, dictionary):
        result = yaml.dump(dictionary, allow_unicode=True, Dumper=yamlloader.ordereddict.CSafeDumper, default_flow_style=False, sort_keys=False)
        #pprint(result)
        return result

    def dict_to_bin(self, dictionary):
        result = json.dumps(dictionary).encode('utf-8') 
        return result

    def bin_to_dict(self, binary):
        result = json.loads(binary.decode('utf-8'))
        return result

    def save_to_db(self, username, binary):
        workflow_object = WorkflowModel(created_by = username, data_bin = binary)
        workflow_object.save()

    def load_from_db(self, uuid):
        workflow_object = WorkflowModel.objects.get(id = uuid)
        return workflow_object

    def memview_to_bin(self, memview):
        result = memview.tobytes()
        return result
    
    def get_last_object(self):
        workflow_object = WorkflowModel.objects.all().aggregate(Max('creation_timestamp'))
        return workflow_object

    def get_all_objects(self):
        objects = WorkflowModel.objects.all()
        return objects

    def render_to_frontend(self, memview):
        binary_obj = self.memview_to_bin(memview)
        dict_obj = self.bin_to_dict(binary_obj)
        str_obj = self.dump_to_yaml(dict_obj)
        return str_obj

    def validate(self, text):
        result = []
        #syntax check
        syntax = {}
        try:
            dict_data = self.load_from_yaml(text)
            #pprint(dict_data)
        except yaml.scanner.ScannerError as error:
            syntax['type'] = 'danger'
            syntax['msg'] = str(error)
        else:
            syntax['type'] = 'success'
            syntax['msg'] = 'Не обнаружено ошибок в синтаксисе'
        finally:
            result.append(syntax)

        
        return json.dumps(result)

    def iterate_flow(self, old_uuid, text, author):
        result = {}
        #print(old_uuid)
        old_version = None
        if old_uuid != 'new':
            #print('---1---')
            old_version = WorkflowModel.objects.get(id = old_uuid)
        else:
            old_uuid = None    
        validate_result = self.validate(text)
        for block in json.loads(validate_result):
            if block['type'] == 'danger':
                result['status'] = 'error'
                result['msg'] = 'В процессе обнаружены ошибки. Проведите валидацию еще раз'
                return json.dumps(result)
        dict_data = self.load_from_yaml(text)
        bin_data = self.dict_to_bin(dict_data)
        new_version = WorkflowModel(created_by = author, branched_from = old_uuid, data_bin = bin_data)
        new_version.save()
        result['status'] = 'success'
        result['msg'] = str(new_version.id)
        return json.dumps(result)
    
    #@northdebug.query_debugger
    def iterate_from_extend_app(self, old_uuid, json_data, author):
        result = {}
        
        #old_version = WorkflowModel.objects.get(id = old_uuid)
        #old_bin_data = self.memview_to_bin(old_version.data_bin)
        #old_dict_data = self.bin_to_dict(old_bin_data)
        #pprint(old_dict_data['blocks'])
        #json_data['blocks'] = old_dict_data['blocks']
        #pprint(json_data)
        bin_data = self.dict_to_bin(json_data)
        new_version = WorkflowModel(created_by = author, branched_from = old_uuid, data_bin = bin_data)
        new_version.save()
        result['status'] = 'success'
        result['msg'] = str(new_version.id)
        return json.dumps(result)

    def iterate_only_blocks_from_extend_app(self, old_uuid, json_data, author):
        result = {}
        
        old_version = WorkflowModel.objects.get(id = old_uuid)
        old_bin_data = self.memview_to_bin(old_version.data_bin)
        old_dict_data = self.bin_to_dict(old_bin_data)
        #pprint(old_dict_data['blocks'])
        json_data['flow'] = old_dict_data['flow']
        #pprint(json_data)
        bin_data = self.dict_to_bin(json_data)
        new_version = WorkflowModel(created_by = author, branched_from = old_uuid, data_bin = bin_data)
        new_version.save()
        result['status'] = 'success'
        result['msg'] = str(new_version.id)
        return json.dumps(result)

    #@northdebug.query_debugger
    def activate_version(self, uid):
        #print(uid)
        try:
            version = WorkflowModel.objects.get(id = uid)
            #print(version)
        except WorkflowModel.DoesNotExist:
            #print(1)
            return 1
        else:
            #versions = WorkflowModel.objects.all()
            WorkflowModel.objects.update(active = False)
            #for v in versions:
            #    v.active = False
            #    v.save()
            version.active = True
            version.save()
            #print(version)
            return None

    def get_active_version(self):
        result = {}
        try:
            versions = WorkflowModel.objects.get(active = True)
        except WorkflowModel.DoesNotExist:
            result['success'] = False
            result['object'] = 'Не обнаружено активной версии!'
        except WorkflowModel.MultipleObjectsReturned:
            result['success'] = False
            result['object'] = 'Обнаружено несколько активных версий!'
        else:
            result['success'] = True
            result['object'] = versions
        finally:
            return result


    def get_flows_from_version(self, active_version_obj=None):
        result = {}
        #bin_obj = self.memview_to_bin(active_version_obj.data_bin)
        #dict_obj = self.bin_to_dict(bin_obj)
        profiles = NewWorkflowModel.objects.filter(active=True)
        result['success'] = True
        result['object'] = profiles
        return result


    def create_merged_flow(self, original_dict_flow, ogrn = None):
        #result = {}
        #pprint(original_dict_flow)
        flow = original_dict_flow
        meta = {}
        meta['meta']={}
        flow.update(meta)
        ####
        good_ogrn = True
        if ogrn:
            for symbol in ogrn:
                if not symbol.isdigit():
                    good_ogrn = False
            if len(ogrn) != 13:
                good_ogrn = False
        else:
            good_ogrn = False
        if good_ogrn:
            CBhnd = CasebookAPI()
            data = CBhnd.get_organization_card(ogrn)
            try:
                flow['meta']['org_name'] = data['shortName']
            except KeyError:
                flow['meta']['org_name'] = "Анонимная компания"

        ####
        userlog = {}
        userlog['log'] = []
        flow.update(userlog)
        for block in flow['blocks']:
            for stage in block['questions']:
                client_data = {}
                client_data['status'] = 'available'
                client_data['linked_to'] = None
        
                client_data['visible'] = stage['always_visible']
                if stage['common_name'] == 'КОНЕЦ ОПРОСА':
                    client_data['visible'] = False

                client_data['result'] = {
                    'type': None,
                    'data': None
                }
                stage['client_data'] = client_data
        
        #pprint(flow)
        return json.dumps(flow, ensure_ascii=False)


    def create_client_flow(self, client_name, version_name, link_name, ogrn):
        result = {}
        if not ogrn.isdigit():
            result['success'] = False
            result['object'] = 'ОГРН должен состоять только из цифр!'
        try:
            client = Client.objects.get(username = client_name)
        except Client.DoesNotExist:
            result['success'] = False
            result['object'] = 'Клиента с таким именем не существует'
            return result
        else:
            flows = None
            
           # active_version = self.get_active_version()
           # if active_version['success']:
          #      flows = active_version['object']
           # else:
           #     return active_version
            
            try:
         
                profile = NewWorkflowModel.objects.get(id=version_name)
                data = self.create_merged_flow(profile.data,ogrn = str(ogrn))
                
                clientflow = ClientFlowModel(client = client, flow_version = version_name, data = data, link_name = link_name, ogrn=ogrn)

                result['flow_name'] = profile.data['name']
                print(result['flow_name'])
                clientflow.save()


               # bin_flows = self.memview_to_bin(flows.data_bin)
                
               # dict_flows = self.bin_to_dict(bin_flows)
               # for flow in dict_flows:
               #     if flow['id'] == version_name:
#
              #          data = self.create_merged_flow(flow)
              #          result['flow_name'] = flow['name']
                
                        #print(flow['name'])
             #           clientflow = ClientFlowModel(client = client, flow_version = version_name, data = data, link_name = link_name, ogrn=ogrn)
              #          #print(1)
              #          clientflow.save()
                        #print(2)
               
            except IntegrityError:
                result['success'] = False
                result['object'] = 'Привязка уже создана'
                return result
            else:
                result['success'] = True
                result['object'] = 'Привязка успешно создана'
                
                return result


    def get_client_flow(self, flow_id):
        result = {}
        try:
            #client = Client.objects.get(id = client_id)
            client_flow = ClientFlowModel.objects.get(id = flow_id)
        #except Client.DoesNotExist:
        #    result['success'] = False
       #    result['object'] = 'Несуществует клиента с указанным ID'
        except ClientFlowModel.DoesNotExist:
            result['success'] = False
            result['object'] = 'Процесс с указанным ID процесс еще не создан'
        except ValidationError:
            result['success'] = False
            result['object'] = 'Процесс с указанным ID процесс еще не создан'
        else:
            result['success'] = True
            obj = {}
            obj['version'] = client_flow.flow_version
            obj['ogrn'] = client_flow.ogrn
            obj['profile'] = json.loads(client_flow.data)
            try:
                obj['org_name'] = obj['profile']['meta']['org_name']
            except KeyError:
                obj['org_name'] = "НЕТ ДАННЫХ"
            
            
            result['object'] = obj
            
        finally:
            return result


    def get_client_workflow_list(self,page=1):
        page_size = 15
        client_flows = ClientFlowModel.objects.all()[(page-1)*page_size:(page*page_size)-1]
        for flow in client_flows:
            loaded = json.loads(flow.data)
            flow.f_name = loaded['name']
            flow.stats = self.count_client_statistics(loaded)




            
        return client_flows


    def get_clients_list(self):
        result = []
        checker = RoleChecker()
        clients = Client.objects.all()
        
        for client in clients:
            if checker.is_boss(client.username):
               
                result.append(client)
        return result


    def save_client_json_to_db(self, json_data, flow_id):
        #bin_data = self.dict_to_bin(json_data)
        try:
          #  client = Client.objects.get(username = client_username)
            flow = ClientFlowModel.objects.get(id = flow_id)
            
            flow.data = json.dumps(json_data, ensure_ascii=False)
            
            flow.save()
            
       # except Client.DoesNotExist:
       #     return False
            
        except ClientFlowModel.DoesNotExist:
            return False
        except Exception as ex:
            print('!!!!!!!!!!!!!!')
            print(ex)
        else:
            return True



    def link_worker_to_stage(self, master_username, slave_username, stage_id, flow_id, to_None = False):
        result = {}
        try:

            flow = self.get_client_flow(flow_id)
            if not flow['success']:
                result = flow
                return result
            #pprint(flow['object'])
            #return
            for block in flow['object']['profile']['blocks']:
                for stage in block['questions']:
                    if str(stage['id']) == str(stage_id):
                        
                        if not to_None:
                            stage['client_data']['linked_to'] = slave_username

                        else:
                            stage['client_data']['linked_to'] = None
                        success = self.save_client_json_to_db(flow['object']['profile'],flow_id)
                        if not to_None:
                             self.log_action(flow_id, master_username, 'message', 'stage' , 'link' , {'stage_id': stage_id, 'linked_to':slave_username, 'stage_common_name':stage['common_name']} )
                        #print(success)
                        result['success'] = success


        except Client.DoesNotExist:
            print(1)

        else:
            return json.dumps(result)



    def mass_link_worker_to_stage(self, master_username, slave_username, stages_id,flow_id, to_None = False, full_blocks = None):
        result = {}
        try:

            flow = self.get_client_flow(flow_id)
            if not flow['success']:
                result = flow
                return result
            #pprint(flow['object']['flow'])
            if not full_blocks:
                full_blocks = []
            for block in flow['object']['profile']['blocks']:
                for stage in block['questions']:
                    #pprint(stages_id)
                    if (str(stage['id']) in stages_id) or (block['id'] in full_blocks):
                        
                        if not to_None:
                            stage['client_data']['linked_to'] = slave_username
                        else:
                            stage['client_data']['linked_to'] = None
                        success = self.save_client_json_to_db(flow['object']['profile'],flow_id)
                        result['success'] = success
            if not to_None:
                
                self.log_action(flow_id, master_username, 'message', 'stage' , 'link' , {'mass': True, 'stage_count': len(stages_id), 'linked_to': slave_username} )
                        #print(success)
            


        except Client.DoesNotExist:
            print(1)

        else:
            return json.dumps(result)



    def get_all_auditors(self):
        
        auditors = Audit.objects.all()
        return auditors

    def link_audit_to_client(self, flow_id, audit_username, to_None = False):
        result = {}
        try:
            flow = ClientFlowModel.objects.get(id = flow_id)
            if to_None:
                flow.auditor = None
            else:
                auditor = Audit.objects.get(username = audit_username)
                flow.auditor = auditor
            flow.save()
        except Audit.DoesNotExist:
            result['success'] = False
            result['object'] = "Аудитор не найден"
        except ClientFlowModel.DoesNotExist:
            result['success'] = False
            result['object'] = "Привязка не найдена"
        except IntegrityError:
            result['success'] = False
            result['object'] = "Ошибка закрепления"
        else:
            result['success'] = True
        finally:
            return json.dumps(result)

    def change_stage_status(self, flow_id, stage_id, new_status, request_user = None):
        result = {}

        flow = self.get_client_flow(flow_id)
        if flow['success']:
            STAGE_COMMON_NAME = None
            for block in flow['object']['profile']['blocks']:
                for stage in block['questions']:
                    if str(stage['id']) == str(stage_id):
                        #print(new_status)
                        #print(stage['client_data']['status'])
                        stage['client_data']['status'] = new_status
                        STAGE_COMMON_NAME = stage['common_name']
            #print(stage_id)
            #pprint(flow['object']['flow'])
            success = self.save_client_json_to_db(flow['object']['profile'],flow_id)
            #print(success)
            result['success'] = success
            if request_user:
                self.log_action(flow_id, request_user, 'message', 'stage' , 'status_change' , {'stage_id': stage_id, 'new_status': new_status, 'stage_common_name':STAGE_COMMON_NAME} )
    
        return result

    def mass_change_stage_status(self, flow_id, stages_id, new_status, request_user = None):
        result = {}

        flow = self.get_client_flow(flow_id)
        if flow['success']:
            #pprint(stages_id)
            for block in flow['object']['profile']['blocks']:
                for stage in block['questions']:
            
                    if str(stage['id']) in stages_id:
                    #print(new_status)
                    #print(stage['client_data']['status'])
                        stage['client_data']['status'] = new_status
                        #print(stage_id)
            #pprint(flow['object']['flow'])
            success = self.save_client_json_to_db(flow['object']['profile'],flow_id)
            if request_user:   
                
                self.log_action(flow_id, request_user, 'message', 'stage' , 'status_change' , {'mass_change': True, 'stage_count': len(stages_id), 'new_status': new_status} )
            
            #print(success)
            result['success'] = success
        
        return json.dumps(result)

    def save_file(self, client_username, f):
        
        storage_dir = "files/clients/"+str(client_username)+"/"
        
        os.makedirs(storage_dir,exist_ok=True)

        storage_path = storage_dir+str(f)
        
        with open(storage_path, 'wb+') as destination:
            for chunk in f.chunks():
                destination.write(chunk)
        return str(f)


    def answer_question(self, request_user, post_data, file_data, flow_id):
        result = {}
        to_make_visible= []
        #pprint(file_data)
        try:
            client = Client.objects.get(username = request_user)
            if client.role_level != Client.MAXIMAL:
                client = client.master
                
            
        except Client.DoesNotExist:
            
            result['success'] = False
            result['object'] = "Пользователь не является клиентом"
        else:
            
            flow = self.get_client_flow(flow_id)

            if not flow['success']:

                return flow
            stage_id = post_data.get('stage')
            #print('======')
            #print(stage_id)
            #print('======')
            STATUS_AFTER_ANSWER = 'paused'
            STAGE_NAME_TO_LOG = None
            #pprint(flow)
            new_stage = None
            for block in flow['object']['profile']['blocks']:
                for stage in block['questions']:
                    #print(stage['id'])
                    if str(stage['id']) == str(stage_id):
                        #pprint(stage)
                        q_type = stage['question_type']
                        STAGE_NAME_TO_LOG = stage['common_name']
                        #setting VISIBLE for stages
                        
                        answer_id = str(post_data.get("answer","")) 
                        #print(answer_id)
                        stage_answer = None
                        for answer in stage['answers']:
                            if answer['id'] == answer_id:
                                stage_answer = answer
                        #print(stage_answer)
                        try:

                            for block_v in flow['object']['profile']['blocks']:
                                
                                for stage_v in block_v['questions']:
                                    #if stage_v['id'] == 'ef0b432c-6ba9-43be-947b-1dbceced198b':
                                    #    print('yes!')
                            #for stage_v in flow['object']['flow']['flow']:
                                    try:
                                        #pprint(stage_answer['nextQuestion'])
                                        if stage_v['id'] == stage_answer['nextQuestion']['id']:
                                           # print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                                            stage_v['client_data']['visible'] = True
                                            to_make_visible.append(stage_v['id'])
                                            #print('==='+stage_v['id']+'====')
                                    except KeyError:
                                        continue
                                    except TypeError:
                                        continue
                                                            
                        except KeyError:
                            #print('error')
                            pass
                        except Exception as ex:
                            #pprint(ex)
                            pass
                        
                        
                        if stage_answer['isStopAnswer']:
                            STATUS_AFTER_ANSWER = 'denied'

                        #print(q_type)
                        rate = 0
                        
                        if q_type == 'plain':
                            answer = post_data.get("answer","")
                            stage['client_data']['result']['type'] = 'text'
                            stage['client_data']['result']['data'] = str(answer)
                        elif q_type == 'radio':
                            
                            #answer_num = str(post_data.get("answer",""))
                            #print(answer_num)
                            #stage_answer = stage['answers'][answer_num]
                            #print(stage_answer['file'])
                            #pprint(stage_answer)
                            try:
                                rate = stage_answer['rate']
                                stage['client_data']['result']['rate'] = rate
                            except TypeError:
                                pass
                            if str(stage_answer['file']) != '0':
                                try:
                                    f = file_data['file']
                                    saved_file = self.save_file(client.username,f)
                                    stage['client_data']['result']['type'] = 'file'
                                    stage['client_data']['result']['data'] = saved_file
                                    stage['client_data']['result']['text'] = stage_answer['text']
                                except KeyError:
                                    stage['client_data']['result']['type'] = 'file'
                                    stage['client_data']['result']['data'] = None
                                    stage['client_data']['result']['text'] = stage_answer['text']
                                try:
                                    stage['client_data']['result']['comment'] = post_data.get('answer-file-comment')
                                except KeyError:
                                    stage['client_data']['result']['comment'] = ""
                                try:
                                    stage['client_data']['result']['official_filename'] = stage_answer['official_filename']
                                except KeyError:
                                    stage['client_data']['result']['official_filename'] = None
                                #pprint(stage['client_data'])
                            else:
                                stage['client_data']['result']['type'] = 'text'
                                stage['client_data']['result']['data'] = stage_answer['text']

                            try:
                                if stage_answer['risks']:
                                    stage['client_data']['risks'] = stage_answer['risks']
                                if stage_answer['recs']:
                                    stage['client_data']['recs'] = stage_answer['recs']
                                if stage_answer['defections']:
                                    stage['client_data']['defections'] = stage_answer['defections']
                            except KeyError:
                                pass
                        elif q_type == 'check':
                            ans_len = len(stage['answers'])
                            answers_from_request = []
                            for i in range(0,ans_len-1):
                                
                                if post_data.get('answer-'+str(i), '') == 'on':
                                    
                                    
                                    answers_from_request.append(stage['answers'][i])
                            stage['client_data']['result']['type'] = 'list'
                            stage['client_data']['result']['data'] = answers_from_request
                        new_stage = stage
                        break
            #pprint(new_stage)
            #pprint(flow['object']['flow']['flow'])
            
            if self.save_client_json_to_db(flow['object']['profile'], flow_id):
                  
                self.change_stage_status(flow_id, stage_id, STATUS_AFTER_ANSWER)
                result['success'] = True
                result['object'] = stage_id
                result['to_make_visible']=to_make_visible
                #print("------")
                #print(to_make_visible)
                self.link_worker_to_stage(client.username, "foobar" , stage_id,flow_id, to_None=True)
                self.log_action(flow_id, request_user, 'message', 'stage' , 'answer' , {'stage_id': stage_id, 'stage_common_name':STAGE_NAME_TO_LOG,'new_status':STATUS_AFTER_ANSWER} )

                
            else:
                result['success'] = False
                result['object'] = "Ошибка сохранения результата"
        finally:
            return result
            
            
    def get_linked_clients(self, audit_username):
        result = {}
        try:
            auditor = Audit.objects.get(username = audit_username)
            linked_clients = ClientFlowModel.objects.filter(auditor = auditor)
        except Audit.DoesNotExist:
            result['success'] = False
            result['object'] = "Аудитор не найден"
        else:
            result['success'] = True
            result['object'] = linked_clients
        finally:
            return result

    def get_autochecks(self, flow_id):
        result = {}
        try:
            client_flow = ClientFlowModel.objects.get(id = flow_id)

        except ClientFlowModel.DoesNotExist:
            result['success'] = False
            result['object'] = "Такой привязки не существует!"
        else:
            ogrn = client_flow.ogrn
            api = CasebookAPI()
            checks = api.getData(ogrn)
            result['success'] = True
            result['object'] = json.loads(checks)[0]
        finally:
            return json.dumps(result)

    def comment_stage(self, data):
        result = {}

        try:

            stage_id = data.get("stage_id")
            flow_id = data.get("flow_id")
            username_auditor = data.get("username_auditor")

        except KeyError:
            result['success'] = False
            result['object'] = "Неверные входные данные"
        else:
            
            flow = self.get_client_flow(flow_id)
            if not flow['success']:
               return flow
            for block in flow['object']['profile']['blocks']:
                for stage in block['questions']:
                    #print(stage['id'])
                    if str(stage['id']) == str(stage_id):
                        
                       # print(data['comment']) 
                        #print(username_auditor)
                        stage['client_data']['comment'] = {}
                        stage['client_data']['comment']['author'] = username_auditor
                        stage['client_data']['comment']['text'] = data['comment']
                        #print('1000')
                        
                        break
                       
            if self.save_client_json_to_db(flow['object']['profile'], flow_id):
                  
                
                result['success'] = True
                result['object'] = "Комментарий добавлен"
                

                
            else:
                result['success'] = False
                result['object'] = "Ошибка сохранения результата"
        finally:
            return json.dumps(result)

    def count_client_rate(self, flow_id, full = False):
        result = {}
        total_rate = 0
        cert_available = True
        have_red = False
        reds = []
        have_yellow = False
        yellows = []
        have_blue = False
        blues = []
        messages = []

        
            
        flow = self.get_client_flow(flow_id)
        if not flow['success']:
            
            return flow
        #pprint(flow['object']['profile']['blocks'])
        for block in flow['object']['profile']['blocks']:
            for stage in block['questions']:
                try:
                    rate = int(stage['client_data']['result']['rate'])
                    total_rate = total_rate + rate
                except KeyError:
                    pass
                if full:
                    #print(stage['common_name'] + "   " + stage['client_data']['status'])
                    if stage['client_data']['status'] == 'denied':
                        have_red = True
                        reds.append(stage)

                    if stage['client_data']['status'] == 'paused':
                        have_yellow = True
                        yellows.append(stage)
                    
                    if stage['client_data']['status'] == 'available':   
                        have_blue = True
                        blues.append(stage)

                

        result['success'] = True
        result['object'] = total_rate

        if full:
            if have_red or have_yellow:
                cert_available = False
            result['object'] = {}
            result['object']['rate'] = total_rate
            result['object']['have_red'] = have_red
            result['object']['have_yellow'] = have_yellow
            result['object']['have_blue'] = have_blue
            result['object']['reds'] = reds
            result['object']['yellows'] = yellows
            result['object']['blues'] = blues
            result['object']['cert_available'] = cert_available

        return result


    def count_client_statistics(self, flow):
        result = {}
        total = 0
        red = 0
        yellow = 0
        green = 0
        blue  = 0
       # flow = self.get_client_flow(flow_id)
        #if not flow['success']: 
        #    return flow
        
        for block in flow['blocks']:
            
            for stage in block['questions']:
                try:

                    if stage['common_name'] != 'КОНЕЦ ОПРОСА':
                        total = total +1
                        if stage['client_data']['status'] == 'denied':
                            red = red +1
                        elif stage['client_data']['status'] == 'paused':
                            yellow = yellow +1
                        elif stage['client_data']['status'] == 'completed':
                            green = green +1
                        elif stage['client_data']['status'] == 'available':
                            blue = blue +1
                        else:
                            pass
                except KeyError:
                    pass
                except Exception as ex:
                    pass
        result['green_pr'] = int((green/total)*100)
        result['yellow_pr'] = int((yellow/total)*100)
        result['red_pr'] = int((red/total)*100)
        result['total'] = total
        result['red'] = red
        result['yellow'] = yellow
        result['green'] = green
        result['blue'] = blue
            
        return result
    
    def log_action(self, flow_id, log_action_user, log_level, log_object, log_event, log_data):
        #client_username [boss1, boss2 ...]
        #log_action_user [boss1, audit1, admin1 ...]
        #log_level [info, message, warning, danger]
        #log_object [cert, stage, subclient, profile ]
        #log_event {cert: creation_request+,   stage: answer+, link+, status_change+,  subclient: creation+, delete+, update, ban+, profile: creation+, audit_link+}
        #log_data: other like [message, stage_id, ...]
        result = {}

            
        flow = self.get_client_flow(flow_id)
        if not flow['success']:
            result = flow
        else:
            logs = flow['object']['profile']['log']
            new_log = {}
            new_log['log_action_user'] = str(log_action_user)
            new_log['log_level'] = str(log_level)
            new_log['log_object'] = str(log_object)
            new_log['timestamp'] = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            new_log['log_event'] = str(log_event)
            new_log['log_data'] = log_data
            logs.append(new_log)
            if self.save_client_json_to_db(flow['object']['profile'], flow_id):
                result['success'] = True
                result['object'] = "Запись добавлена"
            else:
                result['success'] = False
                result['object'] = "Ошибка сохранения"
        
        return result
    
    def get_log_list(self, flow_id):
        result = {}            
        flow = self.get_client_flow(flow_id)
        if not flow['success']:
            result = flow
        else:
            try:
                logs = flow['object']['profile']['log']
                result['success'] = True
                #result['object'] = sorted(logs, key=lambda x: x['timestamp'])
                #pprint(logs)
                logs.reverse()
                result['object'] = logs
                #pprint(logs)
            except KeyError:
                result['success'] = False
                result['object'] = None

        return result

    def get_client_flows(self, username):
        result = {}
        try:
            client = Client.objects.get(username = username)
            flows = ClientFlowModel.objects.filter(client = client)
        except Client.DoesNotExist:
            result['success'] = False
        
        else:
            for flow in flows:
                loaded = json.loads(flow.data)
                flow.f_name = loaded['name']
                flow.stat = self.count_client_statistics(loaded)
 
                
            result['success'] = True
            result['flows'] = flows
            #esult['stats'] = self.count_client_statistics()
           # print(flows[0])
        finally:
            return result

    def get_slave_flows(self, username):
        result = {}
        try:
            client = Client.objects.get(username = username)
            boss = client.master
            flows = ClientFlowModel.objects.filter(client = boss)
        except Client.DoesNotExist:
            result['success'] = False
        else:
            for flow in flows:
                loaded = json.loads(flow.data)
                flow.f_name = loaded['name']
                flow.stat = self.count_client_statistics(loaded)
            result['success'] = True
            result['flows'] = flows
        finally:
            return result

    def get_audit_flows(self, username):
        result = {}
        try:
            auditor = Audit.objects.get(username = username)
            flows = ClientFlowModel.objects.filter(auditor = auditor)
        except Client.DoesNotExist:
            result['success'] = False
        else:
            for flow in flows:
                loaded = json.loads(flow.data)
                flow.f_name = loaded['name']
                flow.stat = self.count_client_statistics(loaded)
            result['success'] = True
            result['flows'] = flows
        finally:
            return result
from .northflow2 import Northflow
from app_cards.cards import Card
from .pdf import ReportPDF
import json
import os
from pprint import pprint

class Report():
    def __init__(self):
        pass

    def non_uploaded_files(self, flow_id):
        #storage_dir = "files/clients/"+str(flow_id)+"/"
        
        #os.makedirs(storage_dir,exist_ok=True)
        card = Card(flow_id)
        client_data = card.get_flow_card()
        
        if not client_data['success']:
            return client_data
        stages_to_report = []
        start_num = 1
        for block in client_data['object']['profile']['blocks']:
            for stage in block['questions']:
                try:
                    need_file_in_ans = False
                    for a in stage['answers']:
                        if str(a['file']) != '0':
                            need_file_in_ans = True
                            
                    
                #stage['client_data']['result']['type'] == 'file'and stage['client_data']['result']['data'] == None
                    if need_file_in_ans  and stage['client_data']['status'] != 'available' and stage['client_data']['result']['official_filename'] !='' :
                        
                        report_block = {}

                        report_block['num'] = start_num
                        try:
                            report_block['official_filename'] = stage['client_data']['result']['official_filename']
                        except KeyError:
                            report_block['official_filename'] = ''
                        try:
                            report_block['comment'] = stage['client_data']['result']['comment']
                        except KeyError:
                            report_block['comment'] = ''
                        
                        stages_to_report.append(report_block)
                        start_num = start_num + 1
                except KeyError:
                    
                    pass
        result = {}
        result['success'] = True
        result['object'] = stages_to_report
        result['id'] = flow_id 
        return result

    def risks_and_recs(self, flow_id):
        #storage_dir = "files/clients/"+str(flow_id)+"/"
        
        #os.makedirs(storage_dir,exist_ok=True)
        card = Card(flow_id)
        client_data = card.get_flow_card()
        
        if not client_data['success']:
            return client_data
        report = []
        block_num = 1
        for block in client_data['object']['profile']['blocks']:
            block_to_report = {}
            block_to_report['block_common_name'] = block['name']
            block_to_report['num'] = block_num
            block_to_report['link'] = "b_" + str(block_num)
            block_to_report['stages_with_risks'] = []
            stage_num = 1
            for stage in block['questions']:
                to_block = {}
                have_fields = True
                to_block['num'] = stage_num
                to_block['link'] = block_to_report['link'] + "_s_" + str(stage_num)
                try:
                    to_block['risks'] = stage['client_data']['risks']
                except KeyError:
                    to_block['risks'] = None
                try:
                    to_block['recs'] = stage['client_data']['recs']
                except KeyError:
                    to_block['recs'] = None
                try:
                    to_block['defections'] = stage['client_data']['defections']
                except KeyError:
                    to_block['defections'] = None
                    have_fields = False
                try:
                    to_block['comment'] = stage['client_data']['comment']['text']
                except KeyError:
                    to_block['comment'] = None    
                        
                if have_fields:
                    block_to_report['stages_with_risks'].append(to_block)
                    stage_num = stage_num + 1
                    
             #   else:
             #       try:
             #           if stage['client_data']['result']['data'] == None:
             #               #print(1)
             #               less_ans = self.get_less_rate_answer(stage['answers'])
             #               #print(less_ans)
             #               to_block['defections'] = less_ans['defections']
             #               to_block['risks'] = less_ans['risks']
             #               to_block['recs'] = less_ans['recs']
             #               to_block['comment'] = None
             #               block_to_report['stages_with_risks'].append(to_block)
             #       except KeyError:
             #           pass
            
                #########    

            if len(block_to_report['stages_with_risks']) != 0:
                block_num = block_num + 1
                report.append(block_to_report)
        result = {}
        
        result['success'] = True
        result['object'] = report
        result['ogrn'] = client_data['object']['ogrn']
        result['org_name'] = client_data['object']['org_name']
        result['flow_id'] = flow_id
        #report_hnd = ReportPDF()
        #print(report)
        #report_result = report_hnd.risks_and_recs(client_data,report)
                
        return result

    def get_less_rate_answer(self, answers_list):
        try:
            less_ans = answers_list[0]
            for a in answers_list:
                if int(a['rate']) < int(less_ans['rate']):
                    less_ans = a
            return less_ans
        except IndexError:
            return {}


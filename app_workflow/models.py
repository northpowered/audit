from django.db import models
import uuid
from django.contrib.postgres.fields import ArrayField, JSONField
from app_index.models import Client, Audit
"""
class WorkflowStageModel(models.Model):
    class Meta:
        ordering = ['height']   

    id = models.AutoField(primary_key=True, editable = True)
    name = models.TextField(default = 'empty_name', null = False, blank = False)
    common_name = models.TextField(default = 'empty_common_name', null = False, blank = False)
    description = models.TextField(null = True, blank = True)
    icon = models.TextField(default = 'fa fa-ban', null = False, blank = False)
    active = models.BooleanField(default = True, null = False)

    height = models.PositiveIntegerField(unique = True, null = False, blank = False)

    in_group = models.BooleanField(default = False, null = False)
    group_name = models.TextField(null = True, blank = True)
    parent_in_group = models.ForeignKey('self', on_delete=models.CASCADE, default = None, null = True, blank = True, related_name='parent_relations')
    
    prev_stages = ArrayField(models.PositiveIntegerField(null = False, blank = False), null = True, default = list, blank = True)
    #sonext_stages = ArrayField(models.PositiveIntegerField(null = False, blank = False), null = True, default = None, blank = True)


    def __str__(self):
        return str(str(self.id)+'_'+self.common_name)


class StageSimpleModel(WorkflowStageModel):
    
    foobar = models.TextField(default = 'foobar', null = True, blank = True)


class StageQuestionModel(WorkflowStageModel):

    QUESTION_TYPES = [
        ('pl', 'Открытый'),
        ('ch', 'Массовый'),
        ('rb','Селекторный'),
    ]

    q_type = models.CharField(max_length=2, choices=QUESTION_TYPES, default='pl')
    answers = ArrayField(models.TextField(null = False, blank = False), null = True, default = list, blank = True)
    need_file = models.BooleanField(default = False, null = False)
    

class WorkflowGraphModel(models.Model):


    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner = models.ForeignKey(Client, on_delete=models.CASCADE, null = False)
    data = JSONField()

    def __str__(self):
        return str(self.owner)
"""
class WorkflowModel(models.Model):

    class Meta:
        ordering = ['-creation_timestamp']

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    creation_timestamp = models.DateTimeField(auto_now_add=True)
    created_by = models.TextField(null = False, blank = False)
    branched_from = models.UUIDField(default = None, null = True)
    data_bin = models.BinaryField(null = False, blank = False)
    description = models.TextField(null = True, blank = True)
    active = models.BooleanField(default = False, null = False)

class NewWorkflowModel(models.Model):
    class Meta:
        ordering = ['order']
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    creation_timestamp = models.DateTimeField(auto_now_add=True)
    update_timestamp = models.DateTimeField(auto_now_add=True)
    name = models.TextField(null = False, blank = False)
    description = models.TextField(null = True, blank = True)
    active = models.BooleanField(default = True, null = False)
    data = JSONField()
    order = models.IntegerField(unique=True, null = False, blank = False)

    def __str__(self):
        return str(self.name)

class ClientFlowModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    link_name = models.TextField(null = False, blank = False, default = "noname")
    ogrn = models.BigIntegerField(default = 0, null = True, blank = True)
    payment = models.BooleanField(default = False, null = False)
    flow_version = models.UUIDField(null = False)
    data = JSONField()
    auditor = models.ForeignKey(Audit, on_delete = models.SET_NULL, blank = True, null = True)

    def __str__(self):
        return str(self.client)
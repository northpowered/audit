# -*- coding: utf8 -*-
import fpdf

fpdf.SYSTEM_TTFONTS = '/usr/share/fonts'

class FlowPDF(fpdf.FPDF):
    
    def header(self):
        # Устанавливаем лого
        self.image('static/img/reporttoplogo.png', 10, 8, 33)
        self.add_font('Times_New_Roman', '', 'static/fonts/Times_New_Roman.ttf', uni=True)
        self.set_font('Times_New_Roman', '', 12)
 
        # Добавляем адрес
        self.cell(100)
        self.cell(0, 5, """Автономная некоммерческая организация
        «Экспертный Центр по разработке, развитию и распространению комплаенс-принципов и стандартов ответственного ведения бизнеса
        «Агентство Комплаенс Стандарт»""", ln=1)
      
 
        # Разрыв линии
        self.ln(20)
 
    def footer(self):
        self.set_y(-10)
 
        self.set_font('Times_New_Roman', '', 8)
 
        # Добавляем номер страницы
        page = 'Страница ' + str(self.page_no()) + '/{nb}'
        self.cell(0, 10, page, 0, 0, 'C')


class ReportPDF():

    def __init__(self):
        pass


    def non_uploaded_files(self, client_data, report_data):
        
        pdf = FlowPDF()
        pdf.alias_nb_pages()
        pdf.add_page()
        pdf.set_font('DejaVu', '', 18)
        pdf.cell(0, 10, 'Отчет по незагруженным файлам', ln=1)
        pdf.cell(0, 10, txt="Привязка клиента: {}".format(client_data['flow_id']), ln=1)
        pdf.ln(10)
        pdf.set_font('Times_New_Roman', '', 14)
        for stage in report_data:
            pdf.set_font('Times_New_Roman', '', 14)
            pdf.multi_cell(0, 10, txt="Вопрос: {}".format(stage['common_name']))
            pdf.set_font('Times_New_Roman', '', 11)
            try:
                pdf.multi_cell(0, 10, txt="Файл: {}".format(stage['client_data']['result']['official_filename']))
            except KeyError:
                pdf.multi_cell(0, 10, txt="Файл: <ИМЯ НЕ ЗАДАНО>")
            pdf.ln(10)
        filename = client_data['flow_id']+'__non_uploaded_files.pdf'
        full_filename = 'files/clients/'+client_data['flow_id']+'/'+filename
        pdf.output(full_filename)
        result = {'flow_id': client_data['flow_id'], 'filename':filename}
        return result

    def risks_and_recs(self, client_data, report_data):
        return
        pdf = FlowPDF()
        pdf.alias_nb_pages()
        #first page
        pdf.add_page()
        pdf.set_font('Times_New_Roman', '', 20)
        pdf.cell(0, 10, 'Отчет по по рискам и рекомендациям', ln=1)
        pdf.cell(0, 10, txt="Привязка клиента: {}".format(client_data['flow_id']), ln=1)
        pdf.ln(10)
        #pdf.set_font('DejaVu', '', 14)
        for block in report_data:
            pdf.set_font('Times_New_Roman', '', 16)
            pdf.multi_cell(0, 10, txt="{}".format(block['block_common_name']))
            pdf.ln(10)
            counter = 1
            for stage in block['stages_with_risks']:
               pdf.set_font('DejaVu', '', 13)
              # pdf.cell(0, 10, txt="п. № {}".format(counter), ln=1)
               pdf.multi_cell(0, 10, txt="{0}. {1}".format(counter,stage['defections']))
               pdf.set_font('DejaVu', '', 11)
               pdf.multi_cell(0, 10, txt="Риски:")
               pdf.multi_cell(0, 10, txt="      {}".format(stage['risks']))
               pdf.multi_cell(0, 10, txt="Рекомендация:")
               pdf.multi_cell(0, 10, txt="      {}".format(stage['recs']))
               pdf.multi_cell(0, 10, txt="Персональный комментарий эксперта:")
               pdf.multi_cell(0, 10, txt="      {}".format(stage['comment']))
               pdf.ln(5)
               counter = counter + 1
        filename = client_data['flow_id']+'__risks_and_recs.pdf'
        full_filename = 'files/clients/'+client_data['flow_id']+'/'+filename
        pdf.output(full_filename)
        result = {'flow_id': client_data['flow_id'], 'filename':filename}
        return result
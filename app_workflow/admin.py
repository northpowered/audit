from django.contrib import admin
from .models import WorkflowModel, ClientFlowModel, NewWorkflowModel
# Register your models here.

admin.site.register(WorkflowModel)
admin.site.register(ClientFlowModel)
admin.site.register(NewWorkflowModel)
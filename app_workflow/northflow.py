from .models import WorkflowGraphModel, WorkflowStageModel, StageSimpleModel, StageQuestionModel
import json
import itertools
import networkx as nx
from  django.db import IntegrityError

class API():

    def __init__(self, id):
        self.id = id

    def activate(self):
        try:
            stage = WorkflowStageModel.objects.get(id = self.id)
            stage.active = not stage.active
            stage.save()
        except WorkflowStageModel.DoesNotExist:
            return 1
    
    def delete(self):
        try:
            stage = WorkflowStageModel.objects.get(id = self.id)
            stage.delete()
        except WorkflowStageModel.DoesNotExist:
            return 1 


class Workflow():

    def __init__(self):
        self.graph = nx.DiGraph()
        self.stages = WorkflowStageModel.objects.all()
        self.stages_simple = StageSimpleModel.objects.all()
        self.stages_questions = StageQuestionModel.objects.all()

    def create_graph(self):
        for s in self.stages:
            self.graph.add_node(s)
            if len(s.prev_stages) != 0:
                for prev in s.prev_stages:
                    p_stage = WorkflowStageModel.objects.get(id=prev)
                    self.graph.add_edge(p_stage,s) 





    def get_base_flow(self):
        self.create_graph()
        return self.stages

    def get_full_flow(self):

        full_list = []
        for s in self.stages_simple:
            stage = {}
            stage['object'] = s
            stage['type'] = 'simple'
            full_list.append(stage)
        for s in self.stages_questions:
            stage = {}
            stage['object'] = s
            stage['type'] = 'question'
            full_list.append(stage)

        #full_list = sorted(full_list, key=lambda WorkflowStageModel: WorkflowStageModel['object']['height'])
        return full_list



    def get_question_types(self):
        types = StageQuestionModel.QUESTION_TYPES
        return types

    def add_stage(self, request, mode):
        name = request.POST.get("name","")
        common_name = request.POST.get("commonname","")
        description = request.POST.get("description","")
        height = int(request.POST.get("height",""))
        raw_prev_stages = request.POST.get("prev_stages","")
        q_type = request.POST.get("q_type","")
        raw_answers  = request.POST.get("answers","")
        

        prev_stages = raw_prev_stages.split(',')
     
        if raw_prev_stages == '':
            prev_stages = []

        answers = []

        for a in raw_answers.split('\n'):

             a = a.strip()

             answers.append(a)



        if name == '':

            return 'Поле СЛУЖЕБНОЕ НАЗВАНИЕ не должно быть пустым!'
        if common_name == '':
            return 'Поле ВИДИМОЕ НАЗВАНИЕ не должно быть пустым!'
        if height == '':
            return 'Поле ПОЗИЦИЯ не должно быть пустым!'


        try:
            if mode == "addsimple":
                stage = StageSimpleModel(name = name,
                common_name = common_name, 
                description = description,
                height = height,
                prev_stages = prev_stages)
                stage.save()
                return None

            if mode == "addquestion":
                q_t = None
                for t in StageQuestionModel.QUESTION_TYPES:
                    if q_type in t:
                        q_t = t[0]
                need_file = False
                if request.POST.get("need_file",None):
                    need_file = True

                
                stage = StageQuestionModel(name = name,
                common_name = common_name, 
                description = description,
                height = height,
                prev_stages = prev_stages,
                q_type = q_t,
                answers = answers,
                need_file = need_file)
                stage.save()
                return None

        except IntegrityError:
            return "Значение ПОЗИЦИЯ должно быть уникальным!"


    def mass_height(self, request):
        new_height = request.POST.get("height","")
        operation = request.POST.get("operation","")
        if not new_height.isdigit():
            return 'Значение должно быть числом'
        for s in self.stages:
            if operation == 'sum':
                s.height = s.height+int(new_height)
            elif operation == 'mul':
                s.height = s.height*int(new_height)
                
            elif operation == 'sub':
                s.height = s.height - int(new_height)
                
            else:
                return "Ошибка выполнения операции"

        try:
            for s in reversed(self.stages):

                s.save()
        except IntegrityError:
            return "Ошибка выполнения операции"

        return None

 
    def validate_flow(self):
        result = []

        stages_id = []
        for stage in self.stages:
            stages_id.append(stage.id)

        #validate root stage
   
        root_stages = []
        for stage in self.stages:
            if len(stage.prev_stages) == 0:
                root_stages.append(stage.id)
        msg = {}
        if len(root_stages) == 0:
            msg['type'] = 'danger'
            msg['msg'] = 'Отсутствует корневой элемент'
            
        elif len(root_stages) == 1:
            msg['type'] = 'success'
            msg['msg'] = 'Корневой элемент присутствует'
            
        else:
            msg['type'] = 'danger'
            msg['msg'] = 'Обнаружено несколько корневых элементов! Их ID:'
            for s in root_stages:
                msg['msg'] = msg['msg'] + str(s) + '  '
        result.append(msg)

        #validate prev_stages
        prev_stages_check = []

        for stage in self.stages:
            prevs = stage.prev_stages
            for p in prevs:
                if p not in stages_id:
                    error_msg = {}
                    error_msg['type'] = 'danger'
                    error_msg['msg'] = 'В этапе id='+str(stage.id)+' связь на несуществующий объект с id='+str(p)
                    prev_stages_check.append(error_msg)
                    result.append(error_msg)

        if len(prev_stages_check) == 0:
            msg = {}
            msg['type']='success'
            msg['msg']='Несуществующих связей не обнаружено'
            result.append(msg)

        result = json.dumps(result)
        return result
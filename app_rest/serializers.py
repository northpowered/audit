from rest_framework import serializers
from app_workflow.northflow2 import Northflow
import json

class FlowDataSerializer(serializers.Field):
    def to_representation(self, value):  #json
        flow = Northflow()
        bin_data = flow.memview_to_bin(value)
        dict_data = flow.bin_to_dict(bin_data)
        ret = dict_data
        return ret

    def to_internal_value(self, data):   #in-model
        flow = Northflow()
        dict_data = json.loads(data)
        bin_data = flow.dict_to_bin(dict_data)
        ret = bin_data
        return ret


class RawFlowSerializer(serializers.Serializer):
    flow = FlowDataSerializer()

class FlowSerializer(serializers.Serializer):
    id = serializers.UUIDField(format='hex_verbose')
    creation_timestamp = serializers.DateTimeField()
    created_by = serializers.CharField()
    branched_from = serializers.UUIDField(format='hex_verbose')
    data_bin = FlowDataSerializer()
    description = serializers.CharField()
    active = serializers.BooleanField()
from datetime import date, datetime
from django.core.exceptions import MultipleObjectsReturned
from app_workflow.models import NewWorkflowModel
import uuid
import json
from app_workflow.northflow2 import Northflow
from pprint import pprint
from app_certs.certmodule import CertOperations
from app_workflow.reports import Report
from app_workflow import northdebug
from dicttoxml import dicttoxml
from django.db.models import Max
class CertHandler():

    def __init__(self):
        pass

    def get_cert_by_id(self, cert_id):
        hnd = CertOperations(send = None)
        result = hnd.get(cert_id)
        return result

    def create_cert_by_request(self, flow_id, who, level, companyname, ogrn, score, certcreatedate, certenddate,profile_name,profile_id):
        hnd = CertOperations(send = None)
        result = {}
        
        certnum = hnd.insert(level = level,
            companyname=companyname,
            ogrn=ogrn,
            certcreatedate=certcreatedate,
            certenddate=certenddate,
            score=score,
            profile_name=profile_name,
            profile_id=profile_id,
            who_create=who)
        
        if hnd.error == None:
            
            
            result['success'] = True
            result['certnum'] = certnum
            
            rhnd = Report()
            
            report = rhnd.risks_and_recs(flow_id)
            
            fhnd = Northflow()
            fhnd.log_action(flow_id,who,'warning','cert','creation_request',{'level':level,'rate':score,'company':companyname,'cert_id':str(certnum), 'report':report})
        else:
            result['success'] = False
            result['details'] = hnd.error
        return result


class FlowHandler():

    def __init__(self):
        pass

    def get_full_list(self):
        flow = Northflow()
        active = flow.get_active_version()
        if active['success']:
            flow_obj = flow.load_from_db(active['object'].id)
            bin_data = flow.memview_to_bin(flow_obj.data_bin)
            dict_data = flow.bin_to_dict(bin_data)
            return dict_data
        else:
            return None

    def get_json_flow_by_id(self,id):
        result = None
        try:
            profile = NewWorkflowModel.objects.get(id=id)
            
        except NewWorkflowModel.DoesNotExist:
            result = None
        except NewWorkflowModel.MultipleObjectsReturned:
            result = None
        else:
            result = profile.data
        finally:
            return result

    def update_json_flow(self, id, data):
        result = {}
        try:
            profile = NewWorkflowModel.objects.get(id=id)
            profile.data = data
            profile.update_timestamp = datetime.now()
            profile.save()
        except NewWorkflowModel.DoesNotExist:
            result['status'] = 'error'
        except NewWorkflowModel.MultipleObjectsReturned:
            result['status'] = 'error'
        except Exception as ex:
            print(ex)
        else:
            result['status'] = 'success'
        finally:
            return result



    def get_full_xml(self):
        xml = dicttoxml(self.get_full_list())
        xml = xml.decode()
        #xmlfile = open("output.xml", "w")
        #xmlfile.write(xml)
        #xmlfile.close()
        return xml
    def get_full_flow_by_id(self, id):
        #full profile returning
        result = {}
        requested_flow = None
        #full_list = self.get_full_list()
        
        #for flow in full_list:
        #    if flow['id'] == str(id):
        #        requested_flow = flow
       #        break
        profile = NewWorkflowModel.objects.get(id=id)
        requested_flow = profile.data
        if not requested_flow:
            result['success'] = False
            result['details'] = 'No such ID'
            return result


        
        result = requested_flow
        return result
    #@northdebug.query_debugger
    def iterate_flow(self, flow):
        flow_hnd = Northflow()
        active_flow = flow_hnd.get_active_version()
        active_version = active_flow['object'].id
        result = flow_hnd.iterate_from_extend_app(active_version, flow, 'extend')
        #print(result)
        load_result = json.loads(result)
        if load_result['status'] == 'success':
            flow_hnd.activate_version(load_result['msg'])
        return result


    def get_flows_list(self):
        #full_list = self.get_full_list()
        result = []
        profiles = NewWorkflowModel.objects.filter(active=True)
        for profile in profiles:
            result.append({'name':profile.name,'id':profile.id,'description':profile.description})
       #for flow in full_list:
        #    position = {}
       #     position['name'] = flow['name']
       #     try:
       #         position['description'] = flow['description']
       #     except KeyError:
        #        pass
        #    position['id'] = flow['id']
       #     result.append(position)
        return result
    
    def get_flow_by_id(self, id):
        #blocks list returning
        result = {}
        blocks = []
        requested_flow = None
        #full_list = self.get_full_list()
        
       #for flow in full_list:
        #    if flow['id'] == str(id):
        #        requested_flow = flow
        #        break
        try:
            profile = NewWorkflowModel.objects.get(id=id)
            
        except Exception as ex:
            requested_flow = None
        else:
            requested_flow = profile.data   
        if not requested_flow:
            result['success'] = False
            result['details'] = 'No such ID'
            return result

        for block in requested_flow['blocks']:
            summary = {}
            summary['id'] = block['id']
            summary['name'] = block['name']
            try:
                summary['order'] = block['order']
            except KeyError:
                pass
            blocks.append(summary)
        result['success'] = True
        result['blocks'] = blocks
        return result
        

    def get_blocks_by_id(self, id):
        full_list = self.get_full_list()
        for flow in full_list:
            if flow['id'] == id:
                return flow['blocks']
        err_msg = {}
        err_msg['details'] = 'No such ID'
        return err_msg

    def update_flow_by_id(self, id, updated_subflow):
        
        full_list = self.get_full_list()
        for flow in full_list:
            if flow['id'] == id:
                #pprint(flow)
                #print(full_list.index(flow))
                index = full_list.index(flow) 
                full_list.pop(index)
                full_list.insert(index, updated_subflow)                
        result = self.iterate_flow(full_list)
        return result
        
    def add_new_subflow(self, subflow_name):
        full_list = self.get_full_list()
        new_subflow = {}
        new_subflow['id'] = str(uuid.uuid4())
        new_subflow['name'] = str(subflow_name)
        new_subflow['description'] = ""
        new_subflow['blocks'] = []
        new_subflow['flow'] = []
        full_list.append(new_subflow)
        self.iterate_flow(full_list)
        result = self.get_flows_list()
        return result

    def delete_subflow(self, id):
        full_list = self.get_full_list()
        for flow in full_list:
            if flow['id'] == id:
                index = full_list.index(flow)
                full_list.pop(index)
        self.iterate_flow(full_list)
        result = self.get_flows_list()
        return result


    
    def profile_get_block_by_id(self, id):
        result = {}
        profiles = NewWorkflowModel.objects.all()
        for profile in profiles:
            data = profile.data
            for block in data['blocks']:
               if str(block['id']) == str(id):
                    result = block
                    break 
        #full_list = self.get_full_list()
        #for profile in full_list:
        #    for block in profile['blocks']:
        #        if str(block['id']) == str(id):
        #            result = block
        return result


    def profile_get_straight_list(self, id):
        result = {}
        questions = []
        flow = self.get_flow_by_id(id)
        if flow['success']:
            for block in flow['blocks']:
                list_block = self.profile_get_block_by_id(block['id'])
                for question in list_block['questions']:
                    summary = {}
                    summary['id'] = question['id']
                    summary['common_name'] = question['common_name']
                    try:
                        summary['stage'] = question['stage']
                    except KeyError:
                        pass
                    questions.append(summary)
            result['success'] = True
            result['questions'] = questions
        else:
            result = flow
        return result


    def profile_create_new(self, data):
        result = {}
        #full_list = self.get_full_list()
        #new_profile = {}
        #new_profile['name'] = data['name']
        #new_profile['id'] = str(uuid.uuid4())
        #new_profile['description'] = ""
        #new_profile['blocks'] = []
        #full_list.append(new_profile)
        #self.iterate_flow(full_list)
        base = {
            'name': data['name'],
            'id': None,
            'description':"",
            'blocks':[]
        }
        max_order = NewWorkflowModel.objects.aggregate(Max('order'))['order__max']
        if not max_order:
            max_order = 0
        new_profile = NewWorkflowModel(name=data['name'],description="",data=base,order=max_order+1)
        new_profile.save()
        new_id = str(new_profile.id)
        data = new_profile.data
        data['id'] = new_id
        new_profile.data = data
        new_profile.save()
        result['profile_id'] = new_id
        return result

    def profile_patch_description(self, id, data):
        result = {}
        result['success'] = False
        result['details'] = 'NoSuchID'
        profile_obj = NewWorkflowModel.objects.get(id=id)
        profile = profile_obj.data
        #full_list = self.get_full_list()
        #for profile in full_list:
        #    if str(profile['id']) == str(id):
        try:
            if data['name'] and data['name']!="":
                profile['name'] = str(data['name'])
                profile_obj.name = str(data['name'])
            if data['description'] and data['description']!="":
                profile['description'] = str(data['description'])
                profile_obj.description  = str(data['description'])
        except KeyError:
            result['success'] = False
            result['details'] = 'KeyError'
        else:
            result['success'] = True
            result['details'] = ""
        profile_obj.data = profile
        profile_obj.update_timestamp = datetime.now()
        profile_obj.save()
        #self.iterate_flow(full_list)
        return result


    def profile_add_block(self, id, new_block):
        new_block_id = str(uuid.uuid4())
        result = {}
        profile = self.get_json_flow_by_id(id)
        
        #full_list = self.get_full_list()
        #for profile in full_list:
        #    if str(profile['id']) == str(id):
                #ORDER counting
        orders = [0,]
        for block in profile['blocks']:
            try:
                orders.append(int(block['order']))  
            except KeyError:
                pass
            except ValueError:
                pass
        max_order = max(orders)
        new_block['order'] = max_order+1
        new_block['id'] = new_block_id
        new_block['questions'] = []
        
        profile['blocks'].append(new_block)
        pprint(profile)
        result = self.update_json_flow(id, profile)
        #full_list = self.update_blocks_order(full_list)
        #self.iterate_flow(full_list)
        result['block_id'] = new_block_id
        return result

    def profile_create_fiction_question(self):
        question = {}
        question['id'] = str(uuid.uuid4())
        question['fiction'] = True
        question['answers'] = []
        question['stage'] = 1
        question['type'] = 'question'
        question['common_name'] = 'КОНЕЦ ОПРОСА'
        question['description'] = 'Закончить прохождение опроса'
        question['risks'] = ''
        question['recs'] = ''
        question['defections'] = ''
        question['question_type'] = 'radio'
        question['always_visible'] = True
        answer_1 = {}
        answer_1['id'] = str(uuid.uuid4())
        answer_1['rate'] = 0
        answer_1['text'] = 'Да'
        answer_1['file'] = 0
        answer_1['isStopAnswer'] = False
        answer_1['ids'] = []
        answer_1['nextQuestion'] = None
        answer_1['recs'] = ''
        answer_1['risks'] = ''
        answer_2 = answer_1
        answer_2['id'] = str(uuid.uuid4())
        answer_2['text'] = 'Нет'
        question['answers'].append(answer_1)
        question['answers'].append(answer_2)
        return question






    def profile_update_by_id(self, id, data):
        #swap blocks
        result = {}
        #check recieved blocks
        try:
            if len(data) < 1:
                result['success'] = False
                result['details'] = 'length less then 1'
                return result
        except ValueError:
            result['success'] = False
            result['details'] = 'ValueError'
            return result
        except TypeError:
            result['success'] = False
            result['details'] = 'TypeError'
            return result
        #end check
        full_list = self.get_full_list()
        if full_list == None:
            result['success'] = False
            result['details'] = 'ProfileFindActiveError'
            return result
        #to_response = []
        order = 1
        profile = self.get_json_flow_by_id(id)
        #for profile in full_list:
       #     if str(profile['id']) == str(id):
        old_blocks = profile['blocks']
        profile['blocks'] = []
        #pprint(old_blocks)
        for new_block in data:
            for old_block in old_blocks:
                if old_block['id'] == new_block['id']:
                    old_block['order'] = order
                    profile['blocks'].append(old_block) 
                    order = order + 1   
                #profile['name'] = data['name']
                #pprint('===========')
                #pprint(profile)
                #to_response = profile['blocks']
        self.update_json_flow(id, profile)
        #self.iterate_flow(full_list)
        
        result = self.get_flow_by_id(id)
        return result

    def profile_delete(self, id):
        result = {}
        profile = NewWorkflowModel.objects.get(id=id)
        profile.delete()
        #full_list = self.get_full_list()
       # for profile in full_list:
        #    if str(profile['id']) == str(id):
        #        index = full_list.index(profile)
        #        full_list.pop(index)
       # self.iterate_flow(full_list)
        result['status'] = '0'
        return result

    def profile_add_question_to_block(self, id, data):
        new_question_id = str(uuid.uuid4())
        result = {}
        to_response = None
        profile_id = None
        profile_data = None
        profile_data_to_update = None
        profiles = NewWorkflowModel.objects.all()
        for profile in profiles:
            profile_data = profile.data
            for block in profile_data['blocks']:
                if str(block['id']) == str(id):
                    profile_id = profile.id

        #full_list = self.get_full_list()
        
        #for profile in full_list:
            #for block in profile['blocks']:
                #if str(block['id']) == str(id):
                    stages_countes = [0,]
                    for question in block['questions']:
                        try:
                            if not question['fiction']:
                                stages_countes.append(int(question['stage']))
                        except ValueError:
                            pass
                        except TypeError:
                            pass
                        except KeyError:
                            stages_countes.append(int(question['stage']))
                    new_question = data
                    new_question['stage'] = max(stages_countes) + 1
                    new_question['id'] = new_question_id
                    block['questions'].append(new_question)
                    to_response = new_question
                    profile_data_to_update = profile_data
                    
            
                    #new_question['answers'] = []
        
        profile_data_to_update = self.update_blocks_order(profile_data_to_update)
        #result_from_linking = self.link_question_to_prev_answers(full_list,new_question_id)
        #if result_from_linking['success']:
        #    full_list = result_from_linking['object']
        self.update_json_flow(profile_id,profile_data_to_update)
        #self.iterate_flow(full_list)
        result['question'] = to_response
        return result

    def profile_update_block_by_id(self, id, data):
        #questions swapping here, bitches
        result = {}
        try:
            if len(data) < 1:
                result['success'] = False
                result['details'] = 'length less then 1'
                return result
        except ValueError:
            result['success'] = False
            result['details'] = 'ValueError'
            return result
        except TypeError:
            result['success'] = False
            result['details'] = 'TypeError'
            return result

        stage_counter = 1
        profile_id = None
        profile_data = None
        profile_data_to_update = None
        profiles = NewWorkflowModel.objects.all()
        for profile in profiles:
            profile_data = profile.data
            for block in profile_data['blocks']:
                if str(block['id']) == str(id):
                    profile_id = profile.id
        #full_list = self.get_full_list()
        #if full_list == None:
        #    result['success'] = False
        #    result['details'] = 'ProfileFindActiveError'
        #    return result
        #for profile in full_list:
        #    for block in profile['blocks']:
       #        if str(block['id']) == str(id):
                    old_questions = block['questions']
                    block['questions'] = []
                    for new_q in data:
                        for old_q in old_questions:
                            if new_q['id'] == old_q['id']:
                                old_q['stage'] = stage_counter
                                block['questions'].append(old_q)
                                stage_counter = stage_counter + 1
                    profile_data_to_update = profile_data
            
                    #block['name'] = data['name']
        #self.iterate_flow(full_list)
        self.update_json_flow(profile_id,profile_data_to_update)
        result = self.profile_get_block_by_id(id)
        return result

    def rename_block_by_id(self, id, data):
        result = {}
        #full_list = self.get_full_list()
        try:
            if not data['name']:
                result['success'] = False
                result['details'] = 'Empty name field'
                return result
        except KeyError:
            result['success'] = False
            result['details'] = 'No name field'
            return result
       # if full_list == None:
       #     result['success'] = False
       #   result['details'] = 'ProfileFindActiveError'
       #     return result
        #stage_counter = 1
        profiles = NewWorkflowModel.objects.all()
        profile_id = None
        profile_data = None
        profile_data_to_update = None
        for profile in profiles:
            profile_data = profile.data
            for block in profile_data['blocks']:
                if str(block['id']) == str(id):
                    profile_id = profile.id
                    block['name'] = str(data['name'])
                    profile_data_to_update= profile_data
        self.update_json_flow(profile_id,profile_data_to_update)
        #self.iterate_flow(full_list)
        result = self.profile_get_block_by_id(id)
        return result



    def profile_delete_block_by_id(self, id):
        result = {}
        #full_list = self.get_full_list()
        profiles = NewWorkflowModel.objects.all()
        profile_id = None
        profile_data = None
        profile_data_to_update = None
        for profile in profiles:
            profile_data = profile.data
            for block in profile_data['blocks']:
                if str(block['id']) == str(id):
                    profile_id = profile.id
                    index = profile_data['blocks'].index(block)
                    profile_data['blocks'].pop(index)
                    profile_data['blocks'] = profile_data['blocks']
                    profile_data_to_update = profile_data
                    
            

        #full_list = self.update_blocks_order(full_list)   
        profile_data_to_update = self.update_blocks_order(profile_data_to_update)
        #pprint(profile_data_to_update)        
        self.update_json_flow(profile_id,profile_data_to_update)
        #self.iterate_flow(full_list)
        result['status'] = '0'
        return result
    

    def profile_get_question_by_id(self, id):
        result = {}
        #full_list = self.get_full_list()
        profiles = NewWorkflowModel.objects.all()
        for profile in profiles:
            profile_data = profile.data
            for block in profile_data['blocks']:
                for question in block['questions']:
                    if str(question['id']) == str(id):
                        result = question
        return result

    def profile_update_question_by_id(self, id, data):
        result = {}
        #full_list = self.get_full_list()
        profiles = NewWorkflowModel.objects.all()
        profile_id = None
        profile_data = None
        profile_data_update = None
        for profile in profiles:
            profile_data = profile.data
            for block in profile_data['blocks']:
                for question in block['questions']:
                    if str(question['id']) == str(id):
                        profile_id = profile.id
                        question.update(data)
                        try:
                            answers = question['answers']
                            for answer in answers:
                                answer['id'] = str(uuid.uuid4())
                            question['answers'] = answers
                        except KeyError:
                            pass
                        result = question
                        profile_data_update = profile_data

        
        result_from_linking = self.link_question_to_prev_answers(profile_data_update, id)
        if result_from_linking['success']:
            profile_data_update = result_from_linking['object']
        #self.iterate_flow(full_list)
        self.update_json_flow(profile_id,profile_data_update)  
        return result

    def profile_add_answer_to_question_by_id(self, id, data):
        result = {}
        new_answer_id = str(uuid.uuid4())
        full_list = self.get_full_list()
        for profile in full_list:
            for block in profile['blocks']:
                for question in block['questions']:
                    if str(question['id']) == str(id):
                        new_answer = data
                        new_answer['id'] = new_answer_id
                        question['answers'].append(new_answer)
        self.iterate_flow(full_list)
        result['answer_id'] = new_answer_id
        return result

    def profile_delete_question_by_id(self, id):
        result = {}
        #full_list = self.get_full_list()
        profile_id = None
        profile_data = None
        profile_data_update = None
        profiles = NewWorkflowModel.objects.all()
        for profile in profiles:
            profile_data = profile.data
            for block in profile_data['blocks']:
                for question in block['questions']:
                    if str(question['id']) == str(id):
                        profile_id = profile.id
                        relink_result = self.relink_question_to_prev_answers_on_delete(block['questions'],id)
                        if relink_result['success']:
                            block['questions'] = relink_result['object']
                        index = block['questions'].index(question)
                        block['questions'].pop(index)
                        profile_data_update = profile_data


        profile_data_update = self.update_blocks_order(profile_data_update)
        self.update_json_flow(profile_id,profile_data_update)
        #self.iterate_flow(full_list)
        result['status'] = '0'
        return result

    def profile_get_answer_by_id(self, id):
        result = {}
        full_list = self.get_full_list()
        profiles = NewWorkflowModel.objects.all()
        for profile in profiles:
            profile_data = profile.data
            for block in profile_data['blocks']:
                for question in block['questions']:
                    for answer in question['answers']:
                        if str(answer['id']) == str(id):
                            result = answer
        return result

    def profile_update_answer_by_id(self, id, data):
        result = {}
        full_list = self.get_full_list()
        for profile in full_list:
            for block in profile['blocks']:
                for question in block['questions']:
                    for answer in question['answers']:
                        if str(answer['id']) == str(id):
                            answer = data
                            answer['id'] = id
        self.iterate_flow(full_list)  
        result['answer_id'] = id
        return result

    def profile_delete_answer_by_id(self, id):
        result = {}
        full_list = self.get_full_list()
        for profile in full_list:
            for block in profile['blocks']:
                for question in block['questions']:
                    for answer in question['answers']:
                        if str(answer['id']) == str(id):
                            index = question['answers'].index(answer)
                            question['answers'].pop(index)
        self.iterate_flow(full_list)        
        result['status'] = '0'
        return result


    def update_blocks_order(self, profile):
        #for profile in full_list:
        b_order = 1
        for block in profile['blocks']:
            block['order'] = b_order
            b_order = b_order + 1
            q_order = 1
            for question in block['questions']:      
                try:
                    if question['fiction']:
                        question['stage'] = 99999
                    else:
                        question['stage'] = q_order
                        q_order = q_order + 1
                except KeyError:
                    question['stage'] = q_order
                    q_order = q_order + 1

        return profile

    def link_question_to_prev_answers(self, profile, question_id):
        result = {}

        for block in profile['blocks']:
            for question in block['questions']:
                if question['id'] == str(question_id):
                    current_stage = question['stage']
                    current_question = question
                    if not isinstance(current_stage, int):
                        if current_stage.isdigit():
                            current_stage = int(current_stage)
                        else:
                            result['success'] = False
                            result['details'] = 'StageTypeError'
                            return result
                    if current_stage < 2:
                        result['success'] = False
                        result['details'] = 'StageNumberError'
                        return result
                    #print(current_stage)
                    
                    needed_stage = current_stage - 1
                    #print(needed_stage)
                    for needed_question in block['questions']:
                        if needed_question['stage'] == needed_stage:
                            for answer in needed_question['answers']:
                                if not answer['nextQuestion']:
                                    answer['nextQuestion'] = {}
                                    answer['nextQuestion']['id'] = str(question_id)
                                    answer['nextQuestion']['common_name'] = current_question['common_name']
                                    answer['nextQuestion']['stage'] = current_question['stage']
                    result['success'] = True
                    result['object'] = profile
        #print(result)
        return result

    def relink_question_to_prev_answers_on_delete(self, questions, id_to_delete):
        result = {}

        for question in questions:
            if question['id'] == str(id_to_delete):
                current_stage = question['stage']
                if not isinstance(current_stage, int):
                    if current_stage.isdigit():
                        current_stage = int(current_stage)
                    else:
                        result['success'] = False
                        result['details'] = 'StageTypeError'
                        return result
                #print(current_stage)
                
                prev_stage = current_stage - 1
                next_stage = current_stage + 1
                #print(prev_stage)
                #print(next_stage)
                if prev_stage < 1:
                    result['success'] = False
                    result['details'] = 'FirstQDeleted'
                    return result
                #print(needed_stage)
                prev_question = None
                next_question = None
               
                for needed_question in questions:
                    if str(needed_question['stage']) == str(prev_stage):
                        prev_question = needed_question
                    if str(needed_question['stage']) == str(next_stage):
                        next_question = needed_question
                #print(prev_question)
                #print(next_question)
                if next_question == None and prev_question != None:
                    for answer in prev_question['answers']:
                        answer['nextQuestion'] = None
                elif next_question != None and prev_question != None:
                    for answer in prev_question['answers']:
                        #answer['nextQuestion'] = str(next_question['id'])
                        answer['nextQuestion'] = {}
                        answer['nextQuestion']['id'] = str(next_question['id'])
                        answer['nextQuestion']['common_name'] = next_question['common_name']
                        answer['nextQuestion']['stage'] = next_question['stage']
                else:
                    result['success'] = False
                    result['details'] = "QuestionsFindError"
                    return result
                for q in questions:
                    if prev_question:
                        if q['id'] == prev_question['id']:
                            q = prev_question
                    if next_question:
                        if q['id'] == next_question['id']:
                            q = next_question

        result['success'] = True
        result['object'] = questions
        #print(result)
        return result
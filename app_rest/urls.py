from django.urls import path
from .views import *
from django.conf.urls import url, include
app_name = "app_rest"
# app_name will help us do a reverse look-up latter.
urlpatterns = [
    #path('workflow/', WorkflowView.as_view()),
    path('workflow/',WorkflowView.as_view()),
    path('workflowclients/',WorkflowClientsView.as_view()),
    path('workflow/<str:id>',WorkflowByIDView.as_view()),
    path('workflowblock/<str:id>',WorkflowBlockView.as_view()),
    path('profiles/',ProfileView.as_view()),
    path('profiles/<str:profile_id>',ProfileByIDView.as_view()),
    path('blocks/<str:block_id>',BlockByIDView.as_view()),
    path('blocks/rename/<str:block_id>',EditBlockByIDView.as_view()),
    path('questions/<str:question_id>',QuestionByIDView.as_view()),
    path('certs/<str:cert_id>',CertApiView.as_view()),
    path('autocheck/',AutocheckApiView.as_view()),
    path('ratecount/<str:flow_id>',RateCountView.as_view()),
    path('test/<str:profile_id>',TestNewView.as_view()),
    url(r'^payment/', include('app_payment.urls')),
   # path('answers/<str:answer_id>',AnswerByIDView.as_view()),
]
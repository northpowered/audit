from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.views import APIView
from app_workflow.northflow2 import Northflow
import json
from .serializers import FlowSerializer, FlowDataSerializer, RawFlowSerializer
from .handlers import FlowHandler, CertHandler
from pprint import pprint
from app_autocheck.casebook import CasebookAPI
from django.http import HttpResponse, response, FileResponse
from rest_framework import pagination
from app_workflow.models import NewWorkflowModel
class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return None


class WorkflowView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request):
        hnd = FlowHandler()
        result = hnd.get_flows_list()
        return Response(result)

    def post(self, request):
        name = request.data.get('name')
        hnd = FlowHandler()
        result = hnd.add_new_subflow(name)
        return Response(result)
    
class WorkflowByIDView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.get_flow_by_id(kwargs['id'])
        return Response(result)

    def post(self, request, **kwargs):
        hnd = FlowHandler()
        flow_data = request.data.get('flow')
        #print(flow_data)
        result = hnd.update_flow_by_id(kwargs['id'], flow_data)
        return Response(result)

    def delete(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.delete_subflow(kwargs['id'])
        return Response(result)




class WorkflowBlockView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.get_blocks_by_id(kwargs['id'])
        return Response(result)

   
    def post(self, request):
        flow_data = request.data.get('blocks')
        flow = Northflow()
        active_flow = flow.get_active_version()
        active_version = active_flow['object'].id
        author = 'extend'
        flow_data = {'blocks':flow_data}
        result = flow.iterate_only_blocks_from_extend_app(active_version,flow_data,author)
        load_result = json.loads(result)
        if load_result['status'] == 'success':
            flow.activate_version(load_result['msg'])
        return Response(result)
    



class ProfileView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)
#+
    def get(self, request):
        hnd = FlowHandler()
        result = hnd.get_flows_list()
        return Response(result)
#+
    def post(self, request):
        hnd = FlowHandler()
        result = hnd.profile_create_new(request.data)
        return Response(result)
#depricate
    def put(self, request):
        hnd = FlowHandler()
        result = hnd.iterate_flow(request.data.get('profiles'))
        return Response(result)
        
    

class ProfileByIDView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)
    #+
    def get(self, request, **kwargs):
        hnd = FlowHandler()
        straight = request.GET.get('straight','')
        if straight == 'list':
            result = hnd.profile_get_straight_list(kwargs['profile_id'])
        elif straight == 'full':
            result = hnd.get_full_flow_by_id(kwargs['profile_id'])
        elif straight == 'json':
            result = hnd.get_full_flow_by_id(kwargs['profile_id'])
            return FileResponse(json.dumps(result),content_type="application/json")
        else:
            result = hnd.get_flow_by_id(kwargs['profile_id'])
        return Response(result)
#+
    def post(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_add_block(kwargs['profile_id'],request.data)
        return Response(result)
#+
    def put(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_update_by_id(kwargs['profile_id'],request.data)
        return Response(result)
#+
    def delete(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_delete(kwargs['profile_id'])
        return Response(result)
#+
    def patch(self, request, **kwargs):
        hnd = FlowHandler()
        data = {}
        data['description'] = request.data.get('description', None)
        data['name'] = request.data.get('name', None)
        result = hnd.profile_patch_description(kwargs['profile_id'],data)
        return Response(result)

class BlockByIDView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)
#+
    def get(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_get_block_by_id(kwargs['block_id'])
        return Response(result)

#+
    def post(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_add_question_to_block(kwargs['block_id'],request.data)
        return Response(result)
#+
    def put(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_update_block_by_id(kwargs['block_id'],request.data)
        return Response(result)
#+
    def delete(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_delete_block_by_id(kwargs['block_id'])
        return Response(result)

class EditBlockByIDView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        #hnd = FlowHandler()
        #result = hnd.profile_get_block_by_id(kwargs['block_id'])
        result = {"pupa":"lupa"}
        return Response(result)
#+
    def put(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.rename_block_by_id(kwargs['block_id'],request.data)
        return Response(result)

class QuestionByIDView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)
#+
    def get(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_get_question_by_id(kwargs['question_id'])
        return Response(result)
#+
    def put(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_update_question_by_id(kwargs['question_id'], request.data)
        return Response(result)

    #TEST IT!
    """
    def post(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_add_answer_to_question_by_id(kwargs['question_id'], request.data)
        return Response(result)
    """
#+
    def delete(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_delete_question_by_id(kwargs['question_id'])
        return Response(result)

class AnswerByIDView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_get_answer_by_id(kwargs['answer_id'])
        return Response(result)

    #TEST IT!
    def put(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_update_answer_by_id(kwargs['answer_id'], request.data)
        return Response(result)

    def delete(self, request, **kwargs):
        hnd = FlowHandler()
        result = hnd.profile_delete_answer_by_id(kwargs['answer_id'])
        return Response(result)


class CertApiView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        hnd = CertHandler()
        result = hnd.get_cert_by_id(kwargs['cert_id'])
        return Response(result)

class RateCountView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        hnd = Northflow()
        result = {}
        full = request.GET.get('full')
        if full:
            full = True
        else:
            full = False
        

        result = hnd.count_client_rate(kwargs['flow_id'], full=full)

        return Response(result)

    def post(self, request, **kwargs):
        hnd = Northflow()
        result = {}
        try:
            available_check = hnd.count_client_rate(kwargs['flow_id'], full=True)
            if available_check['object']['cert_available']:
                #print(available_check['object']['rate'])
                #print(request.POST)
                chnd = CertHandler()
                result = chnd.create_cert_by_request(flow_id = kwargs['flow_id'],
                 who = request.user.username,
                 level=request.POST.get('level',''),
                 companyname=request.POST.get('org',''),
                 ogrn=request.POST.get('ogrn',''),
                 score=available_check['object']['rate'],
                 certcreatedate=request.POST.get('startdate',''),
                 certenddate=request.POST.get('enddate',''),
                 profile_name=request.POST.get('profile_name',''),
                 profile_id=request.POST.get('profile_id',''))
                
                
            else:
                result['success'] = False
                result['details'] = 'Невозможно выдать сертификат. Проверьте результаты процесса клиента'
        except KeyError:
            result['success'] = False
            result['details'] = 'Невозможно выдать сертификат. Проверьте входные данные'
        except Exception as ex:
            pprint(ex)
        finally:
            return Response(result)

class AutocheckApiView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        hnd = CasebookAPI()
        result = hnd.getRisksList()
        return Response(result)

    def post(self, request, **kwargs):
        #hnd = CasebookAPI()
        #result = hnd.getRisksList()
        result ={}
        #print(request.data['stage_id'])
        ogrn = request.data['ogrn']
        hnd = FlowHandler()
        stage = hnd.profile_get_question_by_id(request.data['stage_id'])
        #print(stage)
        if stage['type'] != 'autocheck':
            result = {'error':'wrong stage type'}
        else:
            check_list = []
            for a in stage['answers']:
                check_list.append(a['autocheck']['value'])
            CBhnd = CasebookAPI()
            checked = CBhnd.checkByList(check_list, ogrn)
            result = checked
        return Response(result)

class WorkflowClientsView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        hnd = Northflow()
        page = request.GET.get("page",1)
        result_raw = hnd.get_client_workflow_list(page=int(page))
        result = []
        for f in result_raw:
            row = {
                'id':f.id,
                'username':f.client.username,
                'link_name':f.link_name,
                'ogrn':f.ogrn,
                'f_name':f.f_name,
                'stats':f.stats,
                'flow_version':f.flow_version,
                'auditor':None

            }
            if f.auditor:
                row['auditor'] = f.auditor.username
            else:
                row['auditor'] = None
            result.append(row)
        return Response(result)

class TestNewView(APIView):

    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get(self, request, **kwargs):
        id = kwargs['profile_id']
        profile = NewWorkflowModel.objects.get(id=id)
        result = profile.data
        return Response(result)
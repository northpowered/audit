"""audit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from audit import settings
from app_index import views
from app_get import views as GetViews
from app_qrgen import views as QRViews
from django.conf.urls import url, include
from django.views import generic
from app_media.views import GetMediaFile
from django.views.generic import TemplateView
urlpatterns = [
    path(r'', views.IndexView.as_view()),
    url('qr/', QRViews.QRGeneratorView.as_view()),
    url(r'^certs/', include('app_certs.urls')),
    url(r'^accounts/', include('app_accounts.urls')),
    url(r'^autocheck/', include('app_autocheck.urls')),
    url(r'^workflow/', include('app_workflow.urls')),
    url(r'^card/', include('app_cards.urls')),
    url(r'^test/', include('app_test.urls')),
    url(r'^api/', include('app_api.urls')),
    url(r'^extend/', include('app_extend.urls')),
    url(r'^payment/', include('app_payment.urls')),
    url(r'^search/', include('app_search.urls')),
    path('rest/', include('app_rest.urls')),
    path('media/<str:username>/<str:filename>/', GetMediaFile.as_view()),
    path('get/<str:num>/', GetViews.GetView.as_view()),
    path('login/', views.LoginView.as_view()),
    path('logout/', views.LogoutView.as_view()),
    path('endregister/', views.EndRegView.as_view()),
    path('register/', views.RegisterView.as_view()),
    path('changepassword/', views.ChangePasswordView.as_view()),
    path('resetpassword/', views.ResetPasswordView.as_view()),
    path('confirmation/<str:token>/', views.ConfirmationView.as_view()),
    path('djadmin/', admin.site.urls),
]
